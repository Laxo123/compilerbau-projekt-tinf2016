{
module JavaScanner  (alexScanTokens, Token(..)) where
}
--
-- Autor Jan Wäckers
--
%wrapper "basic"

$digit = 0-9			-- digits
$alpha = [a-zA-Z]       

tokens :-

  $white+      ;
  "//".*       ;
 
  boolean     { \s  ->  BoolTok }
  break		  { \s  ->  BreakTok }
  case		  { \s  ->  CaseTok }
  char 		  { \s  ->  CharTok }
  class		  { \s  ->  ClassTok}
  continue    { \s  ->  ContinueTok }
  default	  { \s  ->  DefaultTok }
  do		  { \s  ->  DoTok }
  else		  { \s  ->  ElseTok }
  for		  { \s  ->  ForTok }
  if		  { \s  ->  IfTok }
  int		  { \s  ->  IntTok }
  instanceOf  { \s  ->  InstOfTok}
  new		  { \s  ->  NewTok }
  private	  { \s  ->  PrivateTok }
  protected	  { \s  ->  ProtectedTok }
  public	  { \s  ->  PublicTok }
  return	  { \s  ->  ReturnTok }
  static	  { \s  ->  StaticTok }
  switch	  { \s  ->  SwitchTok }
  this		  { \s  ->  ThisTok }
  void		  { \s  ->  VoidTok }
  while		  { \s  ->  WhileTok }
  true		  { \s  ->  BoolLit (True) }
  false		  { \s  ->  BoolLit (False) }
  null		  { \s  ->  NullTok }
  $digit+     { \s  ->  IntLit (read s) }
  \'[$alpha $digit]\'             {\s -> CharLit (read s)}
  $alpha [$alpha $digit \_ \']*   { \s -> IdTok s }
  \" .* \"     {\s -> StringLit (read s)}

  \!        { \s -> NotTok}
  \+\+ 		{ \s -> IncTok }
  \-\-		{ \s -> DecTok }
  \(		{ \s -> LBraceTok }
  \)		{ \s -> RBraceTok }
  \{		{ \s -> LBracketTok }
  \}		{ \s -> RBracketTok }
  \=\=		{ \s -> EqualTok }
  \!\=		{ \s -> NotEqualTok }
  \<\=		{ \s -> LessEqualThanTok }
  \>\=		{ \s -> GreaterEqualThanTok }
  \+\=		{ \s -> PlusEqualTok }
  \-\=		{ \s -> MinusEqualTok }
  \*\=		{ \s -> TimesEqualTok }
  \/\=		{ \s -> DivEqualTok }
  \%\=		{ \s -> ModEqualTok }
  \&\=		{ \s -> AndEqualTok }
  \^\=		{ \s -> XorEqualTok }
  \|\=		{ \s -> OrEqualTok }
  \<\<\=    { \s -> SLeftEqualTok }
  \>\>\=    { \s -> SignRShiftEqualTok }
  \>\>\>\=  { \s -> UnsignRShiftEqualTok }
  
  
  \<		{ \s -> LessThanTok }
  \>		{ \s -> GreaterThanTok }
  \&\&		{ \s -> AndTok }
  \|\|		{ \s -> OrTok }
  \+        { \s -> PlusTok}
  \-        { \s -> MinusTok}
  \*        { \s -> MulTok}
  \/        { \s -> DivTok}
  \%        { \s -> ModTok}
 
  
  \&        { \s -> BinAndTok}
  \|        { \s -> BinOrTok}
  \~        { \s -> BinInvertTok}
  \^        { \s -> BinXorTok}
  \<\<      { \s -> LShiftTok}
  \>\>      { \s -> RShiftTok}
  
  \=        { \s -> AssignTok}
  \;        { \s -> SemikolonTok}
  \,        { \s -> CommaTok}
  \.        { \s -> DotTok}
  \:        { \s -> ColonTok}
  \?        { \s -> QuestTok}
  
  
   
 -- [1-9][0-9]*[L]? { }
 --0[xX][0-9a-fA-F]+[L]? { }
{
data Token
  =  BoolTok 
  |  BreakTok 
  |  CaseTok 
  |  CharTok 
  |  ClassTok
  |  ContinueTok 
  |  DefaultTok 
  |  DoTok 
  |  ElseTok 
  |  ForTok 
  |  IfTok 
  |  IntTok 
  |  NewTok 
  |  PrivateTok 
  |  ProtectedTok 
  |  PublicTok 
  |  ReturnTok 
  |  StaticTok 
  |  SwitchTok 
  |  ThisTok 
  |  VoidTok 
  |  WhileTok 
  |  BoolLit Bool 
  |  NullTok 
  |  IntLit Integer 
  |	 CharLit Char
  |  IdTok String
  |	 StringLit String
  |	 NotTok
  |  InstOfTok
	
  |  IncTok 
  |  DecTok 
  |  LBraceTok 
  |  RBraceTok 
  |  LBracketTok 
  |  RBracketTok 
  |  EqualTok 
  |  NotEqualTok 
  |  LessEqualThanTok 
  |  GreaterThanTok
  |  GreaterEqualThanTok 
  |  UnsignRShiftEqualTok
  |  SignRShiftEqualTok
  |  PlusEqualTok 
  |  MinusEqualTok 
  |  TimesEqualTok 
  |  DivEqualTok 
  |  ModEqualTok 
  |  AndEqualTok 
  |  XorEqualTok 
  |  OrEqualTok 
  |  SLeftEqualTok 
  |  LessThanTok 
  |  AndTok 
  |  OrTok 
  |  PlusTok
  |  MinusTok
  |  MulTok
  |  DivTok
  |  ModTok
 
  |  BinAndTok
  |  BinOrTok
  |  BinInvertTok
  |  BinXorTok
  |  LShiftTok
  |  RShiftTok
  
  |  AssignTok
  |  SemikolonTok
  |  CommaTok
  |  DotTok
  |  ColonTok
  |  QuestTok
    deriving (Eq,Show)
	

doScan = do
  s <- getContents
  print (alexScanTokens s)
  
scanFile = do
  s <- readFile "Eingabe.java"
  print (alexScanTokens s)
}