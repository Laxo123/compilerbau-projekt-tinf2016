import java.lang.String;

class Main
{
	public static void main(String[] args)
	{
		System.out.println("Creating instance of TestClass now");
		TestClass a = new TestClass();
		System.out.println("Instance of TestClass successfully created.");
        BClass result = a.myMethod();
	    System.out.println("result != null : "+(result!=null)+" (should be true)");
	}
}