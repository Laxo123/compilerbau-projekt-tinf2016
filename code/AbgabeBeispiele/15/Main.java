import java.lang.String;

class Main
{
	public static void main(String[] args)
	{
		System.out.println("Creating instance of TestClass now");
		TestClass a = new TestClass();
		System.out.println("Instance of TestClass successfully created.");
        System.out.println("10 > 20 : "+a.myMethod(10,20));
		System.out.println("20 > 10 : "+a.myMethod(20,10));
		System.out.println("20 > 20 : "+a.myMethod(20,20));
	}
}