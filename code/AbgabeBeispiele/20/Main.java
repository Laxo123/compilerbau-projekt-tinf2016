import java.lang.String;

class Main
{
	public static void main(String[] args)
	{
		System.out.println("Creating instance of TestClass now");
		TestClass a = new TestClass();
		System.out.println("Instance of TestClass successfully created.");

	    System.out.println("result= "+a.myMethod()+" (should be 6");
	}
}