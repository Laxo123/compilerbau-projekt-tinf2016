import java.lang.String;

class Main
{
	public static void main(String[] args)
	{
		System.out.println("Creating instance of TestClass now");
		TestClass a = new TestClass();
		System.out.println("Instance of TestClass successfully created.");
		System.out.println("Calling TestClass.myMethod(String,int) now.");
		a.myMethod("",42);
		System.out.println("Method TestClass.myMethod(String,int) was called successfully.");
	}
}