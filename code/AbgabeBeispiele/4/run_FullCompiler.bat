@Echo off

cd ..
cd ..


if not exist "CompilerMainWithArgs.exe" (
echo compiling compiler...
ghc -o CompilerMainWithArgs  -main-is CompilerMainWithArgs CompilerMainWithArgs.hs 
)


del /s /q /f *.o
del /s /q /f *.hi


echo Removing old output
del /q /f *.class

echo Running compiler
cd %~dp0
..\..\CompilerMainWithArgs.exe TestClass.java



pause