@Echo off

cd ..
cd ..


if not exist "CompilerMainWithArgs.exe" (
echo compiling compiler...
ghc -o CompilerMainWithArgs  -main-is CompilerMainWithArgs CompilerMainWithArgs.hs 
)


del /q /f *.o
del /q /f *.hi


echo Removing old output
del /q /f TestClass.class

echo Running compiler
cd %~dp0
..\..\CompilerMainWithArgs.exe TestClass.java



pause