import java.lang.String;

class Main
{
	public static void main(String[] args)
	{
		System.out.println("Creating instance of TestClass now");
		TestClass a = new TestClass();
		System.out.println("Instance of TestClass successfully created.");
		System.out.println("Calling TestClass.myMethod(int,int) now.");
		int result = a.myMethod(100,42);
		System.out.println("Method TestClass.myMethod(int,int) was called successfully.");
		System.out.println("Result is: "+result);
		System.out.println("Correct result would be: "+test(100,42));
		
		
	}
	
	private static int test(int a,int b)
	{
		return a+b*b+a/a%b;
	}
}