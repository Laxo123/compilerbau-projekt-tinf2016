module TypedAbsSyntax(javatypecheck) where
import Abstract_syntax.AbsSyn

-- Typecheck
javatypecheck :: [Class] -> [Class] -- Prg -> Prg
javatypecheck [] = []
javatypecheck (Class(typ, fieldDecls, methodDecls):xs) = ((typecheckClass (Class(typ, fieldDecls, methodDecls)) (Class(typ, fieldDecls, methodDecls) : xs)) : javatypecheck xs)

-- Typecheck [Class]
typecheckClass :: Class -> [Class] -> Class
typecheckClass (Class(typ, fieldDecls, methodDecls)) cls =
  let
    correctedFieldDecls = correctFieldDecls fieldDecls
    typedMethodDecls = typecheckMethodDecls methodDecls (Class(typ, correctedFieldDecls, methodDecls)) cls
  in
    (Class(typ, correctedFieldDecls, typedMethodDecls))

-- typecheckFieldDecl / -s wird nicht benoetigt, da FieldDecls in This Klasse weitergegeben werden

-- Typecheck [MethodDecl]
typecheckMethodDecls :: [MethodDecl] -> Class -> [Class] -> [MethodDecl]
typecheckMethodDecls [] this cls = []
typecheckMethodDecls (Method(typ, name, tuples, stmt):xs) this cls = ((typecheckMethodDecl (Method(typ, name, (correctTuples tuples), stmt)) this cls) : (typecheckMethodDecls xs this cls))

-- Typecheck MethodDecl
typecheckMethodDecl :: MethodDecl -> Class -> [Class] -> MethodDecl
typecheckMethodDecl (Method(typ, name, tuples, stmt)) this cls =
  let
    correcttyp = correctType typ 
    typedStmt = typecheckStmt stmt correcttyp tuples this cls
  in
    (Method(correcttyp, name, tuples, typedStmt))

-- Typecheck [Stmt]
typecheckStmts :: [Stmt] -> Type -> [(String, Type)] -> Class -> [Class] -> [Stmt]
typecheckStmts [] returntyp symtab this cls = []
typecheckStmts (stmt:xs) returntyp symtab this cls = 
  if isLocalVarDecl stmt then ((typecheckStmt stmt returntyp ((getTuple stmt) : symtab) this cls) : (typecheckStmts xs returntyp ((getTuple stmt) : symtab) this cls))
  else ((typecheckStmt stmt returntyp symtab this cls) : (typecheckStmts xs returntyp symtab this cls))

-- Typecheck fuer Statements ------------------------------------------------------------------------------------------
typecheckStmt :: Stmt -> Type -> [(String, Type)] -> Class -> [Class] -> Stmt

-- Typecheck Block([Stmt])
typecheckStmt (Block(stmts)) returntyp symtab this cls =
  let
    typedstmts = typecheckStmts stmts returntyp symtab this cls
    typ = upperBound typedstmts symtab this cls
  in
    TypedStmt(Block(typedstmts), typ)

-- Typecheck Return( Expr )
typecheckStmt (Return(expr)) returntyp symtab this cls =
  let
    exp = getTypedExpr expr symtab this cls
    typ = getTypeFromExpr exp
  in
    if returntyp == typ || returntyp == "java/lang/Object" then
      TypedStmt(Return(exp), typ)
    else
      if returntyp == "void" && typ == "null" then
        TypedStmt(Return(exp), returntyp)
      else
        error "Rueckgabewert stimmt nicht ueberein!"

-- Typecheck ReturnEmpty
typecheckStmt ReturnEmpty returntyp symtab this cls =
    TypedStmt(ReturnEmpty, "void")

-- Typecheck While( Expr , Stmt )
typecheckStmt (While(expr, stmt)) returntyp symtab this cls =
  let
    exp = getTypedExpr expr symtab this cls
    stm = typecheckStmt stmt returntyp symtab this cls
  in
    if "boolean" == getTypeFromExpr exp then
      TypedStmt(While(exp,stm), getTypeFromStmt stm)
    else
      error "Expression der Whileschleife benoetigt boolean als Typ!"

-- Typecheck LocalVarDecl(Type, String)
typecheckStmt (LocalVarDecl(typ, string)) returntyp symtab this cls = 
  if typ == "String" then
    TypedStmt(LocalVarDecl("java/lang/String", string), "void")
  else
    TypedStmt(LocalVarDecl(typ, string), "void")

-- Typecheck If(Expr, Stmt , Maybe Stmt)
typecheckStmt (If(be, ifs, Nothing)) returntyp symtab this cls =
  let
    bexp = getTypedExpr be symtab this cls
    ifstmt = typecheckStmt ifs returntyp symtab this cls
  in
    if ((getTypeFromExpr bexp) == "boolean") then
      TypedStmt(If(bexp, ifstmt, Nothing), getTypeFromStmt ifstmt)
    else
     error "boolean erwartet"

typecheckStmt (If(be, ifs, Just elses)) returntyp symtab this cls =
  let
    bexp = getTypedExpr be symtab this cls
    ifstmt = typecheckStmt ifs returntyp symtab this cls
    elsestmt = typecheckStmt elses returntyp symtab this cls
  in
    if getTypeFromExpr bexp == "boolean" then
      TypedStmt(If(bexp, ifstmt, Just elsestmt), getTypeFromStmt elsestmt)
    else
      error "boolean erwartet"

-- Typecheck StmtExprStmt(StmtExpr)
typecheckStmt (StmtExprStmt(stmtExpr)) returntyp symtab this cls =
  let
    stmtExp = typecheckStmtExpr stmtExpr symtab this cls
  in
    TypedStmt(StmtExprStmt(stmtExp), getTypeFromStmtExpr(stmtExp))


-- Typecheck fuer StatementExpressions --------------------------------------------------------------------------------
typecheckStmtExpr :: StmtExpr -> [(String, Type)] -> Class -> [Class] -> StmtExpr

-- Typecheck Assign(Expr, Expr)
typecheckStmtExpr (Assign(expr, exp)) symtab this cls =
  let
    typedexpr = getTypedExpr expr symtab this cls
    typedexp = getTypedExpr exp symtab this cls
  in
    if (getTypeFromExpr typedexpr == getTypeFromExpr typedexp || getTypeFromExpr typedexp == "null") then
      TypedStmtExpr(Assign(typedexpr,typedexp), "void")
    else
      if (isAssign expr) then
        let
          asstyp = getTypeFromAssign expr symtab this cls
        in
          if (asstyp == getTypeFromExpr typedexp || getTypeFromExpr typedexp == "null") then
            TypedStmtExpr(Assign((changeTypeOfAssign typedexpr asstyp),typedexp), "void")
          else
            error "Assign von Assign erwartet gleiche Typen!"
      else
        error "Verschiedene Typen bei der Zuweisung!"

-- Typecheck New(Type, [Expr])
typecheckStmtExpr (New(typ, exprlist)) symtab this cls =
  let
    -- Check ob Parameter mit einem Konstruktor uebereinstimmen
    classname = typeToString typ
    clas = getClassViaName classname cls
    listOfListOfTuples = []
    listOfTuples = getListOfConstructorParams clas listOfListOfTuples
  in
    if (checkConstructorParams exprlist listOfTuples) then
      TypedStmtExpr(New(typ, exprlist), typ)
    else
      error "Kein Konstruktor mit entsprechenden Parametern vorhanden!"

-- Typecheck MethodCall(Expr, String, [Expr])
typecheckStmtExpr (MethodCall(expr, string, exprlist)) symtab this cls =
  let
    typedexpr = getTypedExpr expr symtab this cls
    classtype = getTypeFromExpr typedexpr
    typedexprlist = typecheckExprs exprlist symtab this cls
  in
    if (getClassName this) == classtype then
      let
        method = searchMethodDeclInClass string this
        paramscheck = checkMethodParams method typedexprlist
      in
        if paramscheck == True then
          let
            returntyp = getReturntype method
          in
            TypedStmtExpr(MethodCall(typedexpr, string, typedexprlist), returntyp)
        else
          error "Uebergabeparameter stimmen nicht ueberein!"
    else
      let
        classname = typeToString classtype
        clas = getClassViaName classname cls
        method = searchMethodDeclInClass string clas
        paramscheck = checkMethodParams method typedexprlist
      in
        if paramscheck == True then
          let
            returntyp = getReturntype method
          in
            TypedStmtExpr(MethodCall(typedexpr, string, typedexprlist), returntyp)
        else
          error "Uebergabeparameter stimmen nicht ueberein!"


-- Typecheck [Expr]
typecheckExprs :: [Expr] -> [(String, Type)] -> Class -> [Class] -> [Expr]
typecheckExprs [] symtab this cls = []
typecheckExprs (expr:xs) symtab this cls = ((getTypedExpr expr symtab this cls) : (typecheckExprs xs symtab this cls))


-- Typecheck fuer Expressions -----------------------------------------------------------------------------------------
typecheckExpr :: Expr -> [(String, Type)] -> Class -> [Class] -> Expr

-- Typecheck This
typecheckExpr This symtab this cls =
  let
    typ = getClassName(this)
  in
    TypedExpr(This, typ)

-- Typecheck Super
-- Es gibt keine Vererbung

-- Variablen
-- Typecheck LocalOrFieldVar(String)
typecheckExpr (LocalOrFieldVar(string)) symtab this cls =
  let
    typ = searchInTable string symtab
  in
    if (typ /= Nothing) then
      TypedExpr(LocalVar(string), fromJust typ)
    else
    -- check ob Variable in aktueller Klasse liegt und Typ herausfinden
      let
        typ = searchFieldDeclInClass string this
      in
        if (typ /= Nothing) then
          TypedExpr(InstVar(TypedExpr(This, getClassName this), string), fromJust typ)
        else
          error "Variable nicht deklariert!"

-- Typecheck LocalVar(String)
typecheckExpr (LocalVar(string)) symtab this cls =
  let
    typ = checkLocalVar (LocalVar(string)) symtab
  in
    TypedExpr(LocalVar(string), typ)

-- Typecheck InstVar(Expr, String)
typecheckExpr (InstVar(expr, string)) symtab this cls =
  let
    exp = getTypedExpr expr symtab this cls
    typ = checkLocalVar expr symtab
    name = typeToString typ
    clas = (getClassViaName name cls)
  in
    TypedExpr(InstVar(exp, string), typ)

-- Simple-Expressions
-- Typecheck Unary(String, Expr)
typecheckExpr (Unary(string, expr)) symtab this cls =
  let
    typedexpr = getTypedExpr expr symtab this cls
  in
    if ((getTypeFromExpr typedexpr) == "boolean" && string == "!") then
      TypedExpr(Unary(string, typedexpr), "boolean")
    else
      if ((getTypeFromExpr typedexpr) == "int" && (string == "+" || string == "-")) then
        TypedExpr(Unary(string, typedexpr), "int")
      else
        error "+, - expects int, ! expects boolean"

-- Typecheck Binary(String, Expr, Expr)
typecheckExpr (Binary(string, exprOne, exprTwo)) symtab this cls =
  let
    intexprOne = getTypedExpr exprOne symtab this cls
    intexprTwo = getTypedExpr exprTwo symtab this cls
  in
    if ((getTypeFromExpr intexprOne) == "int" && (getTypeFromExpr intexprTwo) == "int") then
      if (string == "&&" || string == "||" || string == "<" || string == ">" || string == "==" || string == "<=" || string == ">=") then
        TypedExpr(Binary(string, intexprOne, intexprTwo), "boolean")
      else
        if (string == "+" || string == "-" || string == "*" || string == "/" || string == "%") then
          TypedExpr(Binary(string, intexprOne, intexprTwo), "int")
        else
          error "+, -, *, /, %, && or || expected"
    else
      error "int expected"

-- Literalregeln
-- Typecheck Integer(Integer)
typecheckExpr (Integer(int)) symtab this cls = TypedExpr(Integer(int), "int")

-- Typecheck Bool(Bool)
typecheckExpr (Bool(bool)) symtab this cls = TypedExpr(Bool(bool), "boolean")

-- Typecheck Char(Char)
typecheckExpr (Char(char)) symtab this cls = TypedExpr(Char(char), "char")

-- Typecheck String(String)
typecheckExpr (String(string)) symtab this cls = TypedExpr(String(string), "java/lang/String")

-- Typecheck Jnull
typecheckExpr (Jnull) symtab this cls = TypedExpr(Jnull, "null")
-- bei Objekten von Klassen darf auch null zugewiesen werden
-- bei Methoden kann Null uebergeben werden

-- Typecheck StmtExprExpr(StmtExpr)
typecheckExpr (StmtExprExpr(stmtExpr)) symtab this cls =
  let
    stmtExp = typecheckStmtExpr stmtExpr symtab this cls
  in
    TypedExpr(StmtExprExpr(stmtExp), getTypeFromStmtExpr(stmtExp))



-- Suche in Symboltabelle nach Variablenname und gibt den entsprechenden Typ zurueck
searchInTable :: String -> [(Type, String)] -> Maybe Type
searchInTable _ [] = Nothing
searchInTable string ((typ, name):xs) = if string == name then Just typ else searchInTable string xs

-- Suche in Klasse nach Variablenname und gibt den entsprechenden Typ zurueck, Class(Type, [FieldDecl], [MethodDecl])
searchFieldDeclInClass :: String -> Class -> Maybe Type
searchFieldDeclInClass _ (Class(_, [], _)) = Nothing
searchFieldDeclInClass string (Class(_, (FieldDecl(typ, name):xs), _)) = if string == name then Just typ else searchInTable string (fromFieldDeclList xs)

-- Suche in Klasse nach Methodenname und gibt die entsprechende Methodendeklaration zurueck, Method(Type, String,[(Type,String)], Stmt)
searchMethodDeclInClass :: String -> Class -> MethodDecl
searchMethodDeclInClass _ (Class(_, _, [])) = error "Methode nicht deklariert"
searchMethodDeclInClass string (Class(_, _, (Method(typ, name, tuples, stmt):xs))) = if string == name then (Method(typ, name, tuples, stmt)) else searchInMethodDecls string xs

-- Suche in Methodendeklarationstabelle nach Methodenname und gib die entsprechende Methodendeklaration zurueck
searchInMethodDecls :: String -> [MethodDecl] -> MethodDecl
searchInMethodDecls _ [] = error "Methode nicht deklariert"
searchInMethodDecls string (Method(typ, name, tuples, stmt):xs) = if string == name then (Method(typ, name, tuples, stmt)) else searchInMethodDecls string xs

-- Wandle eine Liste von FieldDecl in eine Liste der entsprechenden Tupel um
fromFieldDeclList :: [FieldDecl] -> [(Type, String)]
fromFieldDeclList [] = []
fromFieldDeclList (FieldDecl(typ, string):xs) = ((typ, string):(fromFieldDeclList xs))

-- Ueberpruefe ob Typen der Methodenparameter uebereinstimmen
checkMethodParams :: MethodDecl -> [Expr] -> Bool
checkMethodParams (Method( _, _, [], _)) [] = True
checkMethodParams _ [] = False
checkMethodParams (Method( _, _, ((typ, string):xs), _)) (ex:xss) = if (typ /= "java/lang/Object" && typ /= getTypeFromExpr ex) then False else checkList xs xss

-- Uberpruefe ob Typen in der Liste uebereinstimmen
checkList :: [(Type, String)] -> [Expr] -> Bool
checkList [] [] = True
checkList _ [] = False
checkList [] _ = False
checkList ((typ, _):xs) (expr:xss) = if (typ /= "java/lang/Object" && typ /= getTypeFromExpr expr) then False else checkList xs xss

-- Uberpruefe ob Typen der Expressionliste mit einem Konstruktor uebereinstimmen
checkConstructorParams :: [Expr] -> [[(Type, String)]] -> Bool
checkConstructorParams listOfExpressions [] = False
checkConstructorParams listOfExpressions (listOfTuples:xs) =
  if (length listOfExpressions == length listOfTuples && (checkExpTuples listOfExpressions listOfTuples)) then
    True
  else checkConstructorParams listOfExpressions xs

-- s.o.
checkExpTuples :: [Expr] -> [(Type, String)] -> Bool
checkExpTuples [] [] = True
checkExpTuples (exp:xs) ((typ, _):xss) = if getTypeFromExpr exp == typ then checkExpTuples xs xss else False

-- Gib alle Listen von Tupeln von allen Methoden mit name = classname zurueck
getListOfConstructorParams :: Class -> [[(Type, String)]] -> [[(Type, String)]]
getListOfConstructorParams (Class(_, _, [])) listOfListOfTuples = listOfListOfTuples
getListOfConstructorParams (Class(classname, fieldDecls, (Method(_, name, listOfTuples, _):xs))) listOfListOfTuples =
  if "<init>" == name then
    let
      newListOfListOfTuples = listOfListOfTuples ++ [listOfTuples]
    in
      getListOfConstructorParams (Class(classname, fieldDecls, xs)) newListOfListOfTuples
  else getListOfConstructorParams (Class(classname, fieldDecls, xs)) listOfListOfTuples

-- Lese den Wert eines Maybe aus
fromJust :: Maybe a -> a
fromJust Nothing = error "Maybe.fromJust gibt Nothing zurueck"
fromJust (Just x) = x

-- Wandle Type in String um
typeToString :: Type -> String
typeToString a = a

-- Erhalte Klasse mit entsprechendem Namen aus Klassenpool
getClassViaName :: String -> [Class] -> Class
getClassViaName _ [] = error "Klasse nicht deklariert"
getClassViaName string (Class(name, fieldDecls, methodDecls):xs) =
  if string == name then
    Class(name, fieldDecls, methodDecls)
  else getClassViaName string xs

-- Erhalte Klassenname der Klasse
getClassName :: Class -> String
getClassName (Class(name, _, _)) = name

-- Erhalte Rueckgabewert einer Methode
getReturntype :: MethodDecl -> Type
getReturntype (Method(typ, _, _, _)) = typ

-- Erhalte Typ von einem Tupel aus Typ und String
getTypeFromStringTuple :: (Type, String) -> Type
getTypeFromStringTuple (typ, _) = typ

-- Ueberpruefe ob die Expression in der Symboltabelle vorhanden ist und gib ihren Typ zurueck
checkLocalVar :: Expr -> [(Type, String)] -> Type
checkLocalVar _ [] = error "Klasse nicht instanziiert!"
checkLocalVar (LocalVar(string)) ((typ, sstring):xs) =
  if string == sstring then
    typ
  else checkLocalVar (LocalVar(string)) xs

-- Erhalte das Upper Bound von [Stmt]
upperBound :: [Stmt] -> [(Type, String)] -> Class -> [Class] -> Type
upperBound [] symtab this cls = "void"
upperBound (typedstmt:xs) symtab this cls =
  let
    typ = getTypeFromStmt typedstmt
  in
    if upperBound xs symtab this cls == "void" || typ == upperBound xs symtab this cls then
      typ
    else "java/lang/Object"

-- Ueberpruefe ob es sich beim Statement um eine LocalVarDecl handelt
isLocalVarDecl :: Stmt -> Bool
isLocalVarDecl (LocalVarDecl _) = True
isLocalVarDecl _     = False

-- Ueberpruefe ob es sich beim Statement um einen Block handelt
isBlock :: Stmt -> Bool
isBlock (Block _) = True
isBlock _     = False

-- Ueberpruefe ob es sich beim Statement um einen Return handelt
isReturn :: Stmt -> Bool
isReturn (Return _) = True
isReturn _     = False

-- Erhalte das Tupel aus LocalVarDecl
getTuple :: Stmt -> (Type, String)
getTuple (LocalVarDecl(typ, string)) = ((correctType typ), string)

-- Wandle Strings/Objects in Exprs in java/lang/.. um
getTypedExpr :: Expr -> [(String, Type)] -> Class -> [Class] -> Expr
getTypedExpr expr symtab this cls =
  let
    typedexpr = typecheckExpr expr symtab this cls
  in
    if (getTypeFromExpr typedexpr) == "String" then
      TypedExpr(expr, "java/lang/String")
    else if (getTypeFromExpr typedexpr) == "Object" then
      TypedExpr(expr, "java/lang/Object")
    else
      typedexpr

-- Wandle Strings/Objects in Exprs in java/lang/.. um
correctType :: Type -> Type
correctType typ =
  if typ == "String" then
    "java/lang/String"
  else if typ == "Object" then
    "java/lang/Object"
  else
    typ

-- Wandle Strings/Objects in FieldDecls in java/lang/.. um
correctFieldDecls :: [FieldDecl] -> [FieldDecl]
correctFieldDecls [] = []
correctFieldDecls (FieldDecl(typ, string):xs) =
  if typ == "String" then
    (FieldDecl("java/lang/String", string):(correctFieldDecls xs))
  else if typ == "Object" then
    (FieldDecl("java/lang/Object", string):(correctFieldDecls xs))
  else
    (FieldDecl(typ, string):(correctFieldDecls xs))
    
-- Wandle Strings/Objects in Tupeln in java/lang/.. um
correctTuples :: [(Type, String)] -> [(Type, String)]
correctTuples [] = []
correctTuples ((typ, string):xs) =
  if typ == "String" then
    (("java/lang/String", string):(correctTuples xs))
  else if typ == "Object" then
    (("java/lang/Object", string):(correctTuples xs))
  else
    ((typ, string):(correctTuples xs))

-- Falls Assign gibt es True zurueck
isAssign :: Expr -> Bool
isAssign (StmtExprExpr(Assign(_,_))) = True
isAssign _ = False

-- Gibt Typ des Assign zurueck
getTypeFromAssign :: Expr -> [(String, Type)] -> Class -> [Class] -> Type
getTypeFromAssign (StmtExprExpr(Assign(expr, exp))) symtab this cls = (getTypeFromExpr (getTypedExpr exp symtab this cls))

-- Ändert den Wert des Assign falls es weiterbenutzt wird
changeTypeOfAssign :: Expr -> Type -> Expr
changeTypeOfAssign (TypedExpr (StmtExprExpr (TypedStmtExpr (Assign (expr , exp), _)), _)) typ = 
    (TypedExpr (StmtExprExpr (TypedStmtExpr (Assign (expr , exp), typ)),typ))

-- Gib die Symboltabelle aus
printSymtab :: [(Type, String)] -> String
printSymtab [] = "keine weiteren Variablen"
printSymtab ((typ, string):xs) = (typeToString typ) ++ " " ++ string ++ "; " ++ (printSymtab xs)

-- Testsuite ----------------------------------------------------------------------------------------------------------
test1 = typecheckExpr (Integer(1)) [] (Class("myClass", [], [])) []
test2 = typecheckExpr (String("Hallo")) [] (Class("myClass", [], [])) []
test3 = typecheckExpr (Char('a')) [] (Class("myClass", [], [])) []
test4 = typecheckExpr (Bool(True)) [] (Class("myClass", [], [])) []
test5 = typecheckExpr (Binary("&&", Integer(5), Integer(5))) [] (Class("myClass", [], [])) []
test5b = typecheckExpr (Binary("&&", LocalOrFieldVar("myVar"), Integer(5))) [("int", "myVar")] (Class("myClass", [], [])) []
test6 = typecheckExpr (Unary("!", Integer(3))) [] (Class("myClass", [], [])) []
test7 = typecheckExpr (InstVar(LocalVar("classInstance"), "myVar")) [("myClass", "classInstance")] (Class("thisClass", [], [])) [(Class("myClass", [FieldDecl("int", "myVar")], []))]
test80 = typecheckExpr (LocalOrFieldVar("myVar")) [("int", "myVar")] (Class("myClass", [], [])) []
test81 = typecheckExpr (LocalOrFieldVar("myVar")) [] (Class("myClass", [(FieldDecl("int", "myVar"))], [])) []
test9 = typecheckExpr This [] (Class("myClass", [], [])) []

test10 = typecheckStmtExpr (MethodCall(This, "myMethod", [])) [] (Class("myClass", [], [])) []
test11 = typecheckStmtExpr (MethodCall(This, "myMethod", [])) [] (Class("myClass", [], [Method("int", "myMethod", [], (Return (Integer(1))))])) []
test12 = typecheckStmtExpr (New("myClass2", [])) [] (Class("myClass", [], [])) [(Class("myClass", [], [])), (Class("myClass2", [], [Method("int", "<init>", [], (Return (Integer(1))))]))]
test13 = typecheckStmtExpr (Assign((LocalOrFieldVar("myVar")), (Integer(1)))) [] (Class("myClass", [(FieldDecl("int", "myVar"))], [])) []
test14 = typecheckStmtExpr (Assign((LocalOrFieldVar("myVar")), (Integer(1)))) [("int", "myVar")] (Class("myClass", [], [])) []

-- test20 = typecheckStmt (If (Bool(True), Block([]), Nothing)) [] (Class("myClass", [], [])) []
-- test21 = typecheckStmt (If (Integer(1), Block([]), Nothing)) [] (Class("myClass", [], [])) []
-- test22 = typecheckStmt (If (Bool(True), Block([]), Just (Block([])))) [] (Class("myClass", [], [])) []
-- test23 = typecheckStmt (If (Bool(True), Block([]), Just (Return (Integer(1))))) [] (Class("myClass", [], [])) []
-- test24 = typecheckStmt (If(Bool(True), Return (Integer(1)), Just (Return (Integer(1))))) [] (Class("myClass", [], [])) []
-- test25 = typecheckStmt (While((Binary("&&", Integer(5), Integer(5))), (If(Bool(True), Return (Integer(1)), Just (Return (Integer(1)))))))
-- test26 = typecheckStmt ReturnEmpty
-- test27 = typecheckStmt (Return(Integer(1))) [] (Class("myClass", [], [])) []
-- test28 = typecheckStmt (Block [LocalVarDecl ("int","z"), Return (LocalOrFieldVar "z")]) [] (Class ("DemoClass",[],[Method ("int","testmethod",[],Block [LocalVarDecl ("int","z")])])) []
-- test29 = typecheckStmt (Block [If(Bool(True), Return (Integer(1)), Just (Return (Integer(1)))), If (Bool(True), Block([]), Nothing)]) [] (Class ("DemoClass",[],[Method ("int","testmethod",[],Block [LocalVarDecl ("int","z")])])) []
-- test30 = typecheckStmt (Block [If( Binary("&&", LocalOrFieldVar("myVar"), Integer(5)), Return (Integer(1)), Just (Return (Integer(1)))), If (Bool(True), Block([]), Nothing)]) [("int", "myVar")] (Class ("DemoClass",[],[Method ("int","testmethod",[],Block [LocalVarDecl ("int","z")])])) []


test40 = javatypecheck [Class ("EmptyClass",[],[])]
test41 = javatypecheck [Class ("DemoClass",[],[Method ("int","testmethod",[("int","x"),("int","y")],Return (Integer 2))]),Class ("SndClass",[FieldDecl ("int","a"),FieldDecl ("int","b")],[])]
test42 = javatypecheck [Class ("DemoClass",[],[Method ("int","testmethod",[("int","x"),("int","y")],Block [LocalVarDecl ("int","z"),StmtExprStmt (Assign (LocalOrFieldVar "z",Binary ("+",LocalOrFieldVar "y",Binary ("*",Integer 1,LocalOrFieldVar "x")))),If (Binary ("==",LocalOrFieldVar "x",Integer 1),Block [Return (Integer 1)],Just (Block [If (Binary ("<=",LocalOrFieldVar "x",Integer 20),Block [Return (Integer 2)],Nothing)])),Return (LocalOrFieldVar "z")])]),Class ("SndClass",[FieldDecl ("int","a"),FieldDecl ("int","b")],[])]


tests19a = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")], Nothing)])])]
tests19b = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")], Just (Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"), Block [Return (LocalOrFieldVar "a")], Nothing)]))])])]
tests19c = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")], Just (Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"), Block [Return (Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"))], Nothing)]))])])]
tests19d = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")], Just (Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"), Block [(StmtExprStmt (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"),LocalOrFieldVar "b"])))], Nothing)]))])])]
tests19e = javatypecheck [Class ("TestClass",[],[Method (("int","ggT",[("int","a"),("int","b")], StmtExprStmt (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"),LocalOrFieldVar "b"]))))])]
tests19f = javatypecheck [Class ("TestClass",[FieldDecl("TestClass", "dod")],[Method (("int","ggT",[("int","a"),("int","b")], StmtExprStmt (MethodCall (LocalOrFieldVar "dod","ggT",[Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"),LocalOrFieldVar "b"]))))])]

teststest = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[],Block [LocalVarDecl ("GGT","ggt"),StmtExprStmt (Assign (StmtExprExpr (Assign (LocalOrFieldVar "ggt",StmtExprExpr (New ("GGT",[])))),StmtExprExpr (New ("GGT",[])))),Return (StmtExprExpr (MethodCall (LocalOrFieldVar "ggt","ggT",[Integer 12,Integer 18])))])]), Class ("GGT",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")], Block [])])]

tests1 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block [])])]
tests2 = javatypecheck [Class ("TestClass",[FieldDecl ("String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],Block [])])]
tests3 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("void","myMethod",[],Block [])])]
tests4 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("void","myMethod",[("String","text"),("int","zahl")],Block [])])]
tests5 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[],Block [Return (Integer 42)])])]
tests6 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[("String","text"),("int","zahl")],Block [Return (LocalOrFieldVar "zahl")])])]
tests7 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("String","myMethod",[("String","text"),("int","zahl")],Block [Return (LocalOrFieldVar "text")])])]
tests8 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[("int","a"),("int","b")],Block [Return (Binary ("+",Binary ("+",LocalOrFieldVar "a",Binary ("*",LocalOrFieldVar "b",LocalOrFieldVar "b")),Binary ("%",Binary ("/",LocalOrFieldVar "a",LocalOrFieldVar "a"),LocalOrFieldVar "b")))])])]
tests9 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("boolean","myMethod",[],Block [Return (Unary ("!",Bool False))])])]
tests10 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("Object","myMethod",[],Block [Return (String "Hello World")])])]
tests11 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("void","myMethod",[],Block [LocalVarDecl ("String","a"),StmtExprStmt (Assign (LocalOrFieldVar "a",String "abcd")),LocalVarDecl ("int","b"),StmtExprStmt (Assign (LocalOrFieldVar "b",Integer 42)),LocalVarDecl ("char","c"),StmtExprStmt (Assign (LocalOrFieldVar "c",Char 'c')),LocalVarDecl ("boolean","d"),StmtExprStmt (Assign (LocalOrFieldVar "d",Bool True))])])]
tests12 = javatypecheck [Class ("TestClass",[FieldDecl ("String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],Block []),Method ("String","getText",[],Block [Return (LocalOrFieldVar "text")]),Method ("int","getZahl",[],Block [Return (LocalOrFieldVar "zahl")]),Method ("char","getZeichen",[],Block [Return (LocalOrFieldVar "zeichen")]),Method ("boolean","getBool",[],Block [Return (LocalOrFieldVar "bool")])])]
tests13 = javatypecheck [Class ("TestClass",[FieldDecl ("String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],Block []),Method ("void","setText",[("String","value")],Block [StmtExprStmt (Assign (LocalOrFieldVar "text",LocalOrFieldVar "value"))]),Method ("void","setZahl",[("int","value")],Block [StmtExprStmt (Assign (LocalOrFieldVar "zahl",LocalOrFieldVar "value"))]),Method ("void","setZeichen",[("char","value")],Block [StmtExprStmt (Assign (LocalOrFieldVar "zeichen",LocalOrFieldVar "value"))]),Method ("void","setBool",[("boolean","value")],Block [StmtExprStmt (Assign (LocalOrFieldVar "bool",LocalOrFieldVar "value"))])])]
tests14 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("boolean","myMethod",[("boolean","value")],Block [If (LocalOrFieldVar "value",Block [Return (Bool False)],Nothing),Return (Bool True)])])]
tests15 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("boolean","myMethod",[("int","a"),("int","b")],Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (Bool True)],Just (Block [Return (Bool False)]))])])]
tests16 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[],Block [LocalVarDecl ("int","counter"),StmtExprStmt (Assign (LocalOrFieldVar "counter",Integer 0)),While (Binary ("<",LocalOrFieldVar "counter",Integer 10),Block [StmtExprStmt (Assign (LocalOrFieldVar "counter",Binary ("+",LocalOrFieldVar "counter",Integer 1)))]),Return (LocalOrFieldVar "counter")])])]
tests17 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","methodA",[],Block [Return (StmtExprExpr (MethodCall (This,"methodB",[])))]),Method ("int","methodB",[],Block [Return (Integer 42)])])]
tests18 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("BClass","myMethod",[],Block [Return (StmtExprExpr (New ("BClass",[])))])]),Class ("BClass",[],[Method ("void","<init>",[],Block [])])]
tests19 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")],Just (Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (StmtExprExpr (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"),LocalOrFieldVar "b"])))],Just (Block [Return (StmtExprExpr (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "b",LocalOrFieldVar "a"),LocalOrFieldVar "a"])))]))]))])])]
tests20 = javatypecheck [Class ("TestClass",[],[Method ("void","<init>",[],Block []),Method ("int","myMethod",[],Block [LocalVarDecl ("GGT","ggt"),StmtExprStmt (Assign (LocalOrFieldVar "ggt",StmtExprExpr (New ("GGT",[])))),Return (StmtExprExpr (MethodCall (LocalOrFieldVar "ggt","ggT",[Integer 12,Integer 18])))])]),Class ("GGT",[],[Method ("void","<init>",[],Block []),Method ("int","ggT",[("int","a"),("int","b")],Block [If (Binary ("==",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (LocalOrFieldVar "a")],Just (Block [If (Binary (">",LocalOrFieldVar "a",LocalOrFieldVar "b"),Block [Return (StmtExprExpr (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "a",LocalOrFieldVar "b"),LocalOrFieldVar "b"])))],Just (Block [Return (StmtExprExpr (MethodCall (This,"ggT",[Binary ("-",LocalOrFieldVar "b",LocalOrFieldVar "a"),LocalOrFieldVar "a"])))]))]))])])]




