module Descriptor(getTypeDescriptor,getFieldDescriptor,getMethodDescriptor) where
import Abstract_syntax.AbsSyn

getTypeDescriptor:: String -> String
getTypeDescriptor "byte" = "B"
getTypeDescriptor "char" = "C"
getTypeDescriptor "double" = "D"
getTypeDescriptor "float" = "F"
getTypeDescriptor "int" = "I"
getTypeDescriptor "long" = "J"
getTypeDescriptor "short" = "S"
getTypeDescriptor "boolean" = "Z"
getTypeDescriptor "void" = "V"
getTypeDescriptor className = "L"++className++";"


getFieldDescriptor:: String ->String
getFieldDescriptor = getTypeDescriptor

getMethodDescriptor:: MethodDecl -> String
getMethodDescriptor (Method(returnTypeName, _,parameters, _)) = "(" ++ (concat(map getTypeDescriptor (map fst parameters)))++ ")" ++ (getTypeDescriptor returnTypeName)