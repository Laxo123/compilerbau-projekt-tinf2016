{
module JavaParser (parser) where
import Abstract_syntax.AbsSyn
import JavaScanner

-- 
-- Autor: Jan Wäckers
--
}

%name parse
%tokentype { Token }
%error { parseError }
  
%token 
    BOOLEAN { BoolTok }
    BREAK { BreakTok }
    -- CASE{ CaseTok } 
    CHAR  { CharTok  }
    CLASS { ClassTok}
          
    CONTINUE {ContinueTok}
    DEFAULT {DefaultTok} 
    DO      {DoTok} 
    ELSE    {ElseTok} 
    FOR     {ForTok} 
    IF      {IfTok} 
    INT     {IntTok} 
    NEW     {NewTok} 
    PRIVATE {PrivateTok} 
    PROTECTED {ProtectedTok} 
    PUBLIC    {PublicTok} 
    RETURN    {ReturnTok} 
    STATIC    {StaticTok}
    JNULL     {NullTok} 
    -- SwitchTok 
    THIS      {ThisTok} 
    VOID      {VoidTok }
    WHILE     {WhileTok }
    BOOLLITERAL {BoolLit $$}
    CHARLITERAL {CharLit $$}
    INTLITERAL  { IntLit $$}
    IDENTIFIER  { IdTok $$}
    STRINGLITERAL {StringLit $$}
    INSTANCEOF  {InstOfTok}  
    
    EXCLMARK    { NotTok}
    INCREMENT   {IncTok} 
    DECREMENT   {DecTok} 
    LBRACE      {LBraceTok }
    RBRACE      {RBraceTok} 
    LBRACKET    {LBracketTok} 
    RBRACKET    {RBracketTok} 
    EQUAL                   {EqualTok} 
    NOTEQUAL                {NotEqualTok} 
    LESSEQUAL               {LessEqualThanTok} 
    GREATEREQUAL            {GreaterEqualThanTok} 
    UNSIGNEDSHIFTRIGHTEQUAL {UnsignRShiftEqualTok}
    SIGNEDSHIFTRIGHTEQUAL   {SignRShiftEqualTok}  
       
    PLUSEQUAL               {PlusEqualTok} 
    MINUSEQUAL              {MinusEqualTok} 
    TIMESEQUAL              {TimesEqualTok} 
    DIVIDEEQUAL             {DivEqualTok} 
    MODULOEQUAL             {ModEqualTok} 
    ANDEQUAL                {AndEqualTok} 
    XOREQUAL                {XorEqualTok} 
    OREQUAL                 {OrEqualTok} 
    LESS         {LessThanTok} 
    GREATER      {GreaterThanTok}
    LOGICALAND   {AndTok} 
    LOGICALOR    {OrTok} 
    
    PLUS         {PlusTok}
    MINUS        {MinusTok}
    MUL          {MulTok}
    DIV          {DivTok}
    MOD          {ModTok}
    
 
    AND          {BinAndTok}
    OR           {BinOrTok} 
    TILDE        {BinInvertTok}
    XOR          {BinXorTok}
    SHIFTLEFTEQUAL {LShiftTok} 
    -- RShiftTok
  
    ASSIGN       {AssignTok}
    SEMICOLON    {SemikolonTok}
    COMMA        {CommaTok}
    DOT          {DotTok}
    COLON        {ColonTok}
    QUESMARK     {QuestTok}
    

    -- kein for,Switch/Case

%%

compilationunit  : typedeclarations { $1 }

typedeclarations : typedeclaration { [$1] }
                 | typedeclarations typedeclaration { $1 ++ [$2] }

typedeclaration  : classdeclaration { $1 }

-- classbody: ([Fields],[Methods])
-- hier nach Konstruktor checken insertConstr $2 (snd $3))

classdeclaration : CLASS IDENTIFIER classbody { Class($2,fst $3,insertConstr $2 (snd $3) ) }
                 | modifiers CLASS IDENTIFIER classbody {Class($3, [], []) }

classbody        : LBRACKET RBRACKET  { ([], []) }
                 | LBRACKET classbodydeclarations  RBRACKET { $2 }

modifiers        : modifier { }
                 | modifiers modifier { }

                 -- Fields und Methods an dieser Stelle trennen 
classbodydeclarations :  classbodydeclaration { $1}
                      | classbodydeclarations classbodydeclaration{ rule_classbodydeclarations $1 $2}
                    

modifier         : PUBLIC { }
                 | PROTECTED { }
                 | PRIVATE { }
                 | STATIC { }
                

classtype        : classorinterfacetype{ $1 }


classbodydeclaration : classmemberdeclaration { $1}
                     | constructordeclaration {([],[$1])}

classmemberdeclaration : fielddeclaration {([$1],[]) }
                       | methoddeclaration {([],[$1]) }

constructordeclaration : constructordeclarator constructorbody {Method ("void",fst $1,snd $1,$2) }
                       |  modifiers constructordeclarator constructorbody { Method ("void",fst $2,snd $2,$3)}

fielddeclaration : type variabledeclarators  SEMICOLON {FieldDecl($1,fst $2)  }
                  | modifiers type variabledeclarators  SEMICOLON { FieldDecl($2,fst $3)}
                              
methoddeclaration : methodheader methodbody { Method(fst3 $1,snd3 $1,third3 $1,$2)}

constructordeclarator :  simplename LBRACE  RBRACE  { ($1,[]) }
                      |  simplename LBRACE formalparameterlist RBRACE  { ($1,$3)}

                     
                      
constructorbody     : LBRACKET RBRACKET { Block[]}
                 | LBRACKET explicitconstructorinvocation  RBRACKET {Block ([$2]) }
                 | LBRACKET blockstatements  RBRACKET {Block $2 }
                 | LBRACKET explicitconstructorinvocation blockstatements RBRACKET { Block ([$2] ++ $3)}

                 -- methodheader = (type,id,formalparameterlist)
                 
methodheader     : type methoddeclarator {($1,fst $2,snd $2) }
         | modifiers type methoddeclarator { ($2,fst $3,snd $3)}
         | VOID methoddeclarator { ("void",fst $2,snd $2)}
         | modifiers VOID methoddeclarator { ("void",fst $3,snd $3)}


variabledeclarators : variabledeclarator { $1}
                    | variabledeclarators  COMMA  variabledeclarator { ("nop",EmptyStmt)}

methodbody  : block { $1}
            | SEMICOLON { Block[]}

block            : LBRACKET   RBRACKET {Block[] }
                 | LBRACKET  blockstatements  RBRACKET { Block $2 }    
                 
--  EmptyStmt an dieser Stelle entfernen
                 
blockstatements  : blockstatement {removeEmptyStmt $1 }
                 | blockstatements blockstatement { $1 ++ (removeEmptyStmt $2) }
                 
-- localvariabledeclarationstatement = LocalVarDecl

blockstatement     : localvariabledeclarationstatement { splitInitialization $1 }
                 | statement  { [$1] }
                 
                 -- formalparameter = [(type,id)]

formalparameterlist :formalparameter {$1 }
                    | formalparameterlist  COMMA  formalparameter{ $1 ++ $3}

                  
-- this() = MethodCall
explicitconstructorinvocation : THIS LBRACE  RBRACE   SEMICOLON  {StmtExprStmt (MethodCall (This,"<init>",[])) }
                              | THIS LBRACE argumentlist  RBRACE   SEMICOLON
                                {StmtExprStmt (MethodCall (This,"<init>",$3)) }

-- classtypelist    : classtype { }
--                  | classtypelist  COMMA  classtype { }

                 -- methoddeclarator = (id,[paras])
                 
methoddeclarator : IDENTIFIER LBRACE  RBRACE  { ($1,[])}
                 | IDENTIFIER LBRACE formalparameterlist  RBRACE  {($1,$3) }
                 
type     : primitivetype { $1} 
         | referencetype { $1 }

primitivetype    : BOOLEAN { "boolean"}
                 | numerictype { $1  }


referencetype    : classorinterfacetype { $1}

-- ursprünglich: classorinterfacetype : name. abgeändert, da packages nicht unterstützt
classorinterfacetype : simplename{ $1 }

name             : qualifiedname { $1 }
                 | simplename { [$1]}

qualifiedname    : name  DOT IDENTIFIER { $1 ++ [$3] }

simplename       : IDENTIFIER { $1 }

-- = (id,stmt)

variabledeclarator : variabledeclaratorid {($1,EmptyStmt) }
                   | variabledeclaratorid ASSIGN variableinitializer { ($1, StmtExprStmt( Assign (LocalOrFieldVar ($1), $3)))}


                 
                 -- returns [(type,id)]

formalparameter  : type variabledeclaratorid {[($1,$2)] }

argumentlist     : expression { [$1] }
                 | argumentlist  COMMA  expression {$1 ++ [$3]}

numerictype      : integraltype { $1}

variabledeclaratorid : IDENTIFIER {$1 }

variableinitializer  : expression { $1 }

localvariabledeclarationstatement : localvariabledeclaration  SEMICOLON  { $1}

statement    : statementwithouttrailingsubstatement{ $1 }
             | ifthenstatement { $1}
             | ifthenelsestatement {$1}
             | whilestatement { $1 }
                     

expression       : assignmentexpression {$1 }

integraltype     : INT  {"int" }
                 | CHAR { "char"}

-- = LocalVarDecl(type,id)                 
localvariabledeclaration : type variabledeclarators { ($1,$2)}

statementwithouttrailingsubstatement : block { $1}
                                     | emptystatement { EmptyStmt}
                                     | expressionstatement {$1}
                                     | returnstatement { $1}

-- check if statement is not a block
                                     
ifthenstatement  : IF LBRACE expression  RBRACE  statement { If ($3,putStmtIntoBlock $5,Nothing)}

ifthenelsestatement : IF LBRACE expression  RBRACE statementnoshortif 
                      ELSE statement  { If ($3,putStmtIntoBlock $5, Just (putStmtIntoBlock $7)) }

whilestatement   : WHILE LBRACE expression  RBRACE  statement { While ($3,putStmtIntoBlock $5)}

-- assignmentexpression = Expr

assignmentexpression : conditionalexpression { $1}
                     | assignment{ StmtExprExpr ($1) }

emptystatement     :  SEMICOLON  { }

--hier wird Stmt in StmtExprStmt eingepackt

expressionstatement : statementexpression  SEMICOLON {StmtExprStmt($1)}

returnstatement  : RETURN  SEMICOLON  {ReturnEmpty }
                 | RETURN expression  SEMICOLON {Return ($2) }

statementnoshortif : statementwithouttrailingsubstatement { $1}
                   | ifthenelsestatementnoshortif { $1 }
                   | whilestatementnoshortif { $1}

conditionalexpression : conditionalorexpression {$1 }
                      | conditionalorexpression QUESMARK expression  COLON  conditionalexpression { Jnull}

-- hier mit Fkt Assign konstruieren
--  assignment: "id/name" "operator" expr
                      
assignment       :lefthandside assignmentoperator assignmentexpression { rule_assignment $1 $2 $3 }
    
-- return type = expr
statementexpression : assignment {$1 }
                    | preincrementexpression {Assign(Jnull,Jnull) }
                    | predecrementexpression { Assign(Jnull,Jnull)}
                    | postincrementexpression { $1}
                    | postdecrementexpression { $1}
                    | methodinvocation { $1 }
                    | classinstancecreationexpression {$1 }


ifthenelsestatementnoshortif :IF LBRACE expression  RBRACE  statementnoshortif
                  ELSE statementnoshortif  { If ($3, putStmtIntoBlock $5, Just (putStmtIntoBlock $7))}

whilestatementnoshortif : WHILE LBRACE expression  RBRACE  statementnoshortif { While ($3,putStmtIntoBlock $5)}

-- LOGICALOR == ||
conditionalorexpression : conditionalandexpression { $1 }
                        | conditionalorexpression LOGICALOR conditionalandexpression{ Binary ("||",$1,$3)}

lefthandside     : name { rule_lefthandside $1}

assignmentoperator : ASSIGN{ "="}
         | TIMESEQUAL {"*=" }
         | DIVIDEEQUAL { "/="}
         | MODULOEQUAL { "%="}
         | PLUSEQUAL { "+="} 
         | MINUSEQUAL {"-=" }
         | SHIFTLEFTEQUAL { "nop" }
         | SIGNEDSHIFTRIGHTEQUAL { "nop" }
         | UNSIGNEDSHIFTRIGHTEQUAL {"nop" }
         | ANDEQUAL { "nop" }
         | XOREQUAL { "nop"}
         | OREQUAL{ "nop" }

preincrementexpression : INCREMENT unaryexpression { }

predecrementexpression : DECREMENT unaryexpression { }

postincrementexpression : postfixexpression INCREMENT {Assign ($1, Binary ("+", $1, Integer 1)) }

postdecrementexpression : postfixexpression DECREMENT {Assign ($1, Binary ("-", $1, Integer 1))}

methodinvocation : name LBRACE   RBRACE  { MethodCall ( fst (parseName $1),snd (parseName $1) ,[])}
                 | name LBRACE argumentlist RBRACE { MethodCall ( fst (parseName $1),snd (parseName $1),$3)}
                 | primary  DOT IDENTIFIER LBRACE RBRACE  { MethodCall ($1,$3,[])}
                 | primary  DOT IDENTIFIER LBRACE argumentlist  RBRACE  { MethodCall ($1,$3,$5) }
     
classinstancecreationexpression : NEW classtype LBRACE   RBRACE  { New ($2,[])}
                 | NEW classtype LBRACE  argumentlist  RBRACE  {New ($2,$4) }

conditionalandexpression : inclusiveorexpression { $1}
                           | conditionalandexpression LOGICALAND inclusiveorexpression {Binary ("&&",$1,$3)}

fieldaccess      : primary  DOT IDENTIFIER { InstVar ($1,$3) }

unaryexpression     : preincrementexpression { Jnull}
         | predecrementexpression { Jnull}
         | PLUS unaryexpression { Unary ("+", $2)}
         | MINUS unaryexpression { Unary ("-", $2)}
         | unaryexpressionnotplusminus { $1}

         
postfixexpression : primary { $1 }
         | name { rule_postfixexpression $1 }
         | postincrementexpression {StmtExprExpr ($1) }
         | postdecrementexpression{ StmtExprExpr ($1)}
         
-- this, field und klassenzugriff

primary         :  primarynonewarray {$1 }


-- OR == |

inclusiveorexpression : exclusiveorexpression {$1 }
         | inclusiveorexpression OR exclusiveorexpression {Binary ("|",$1,$3) }

primarynonewarray : literal { $1}
         | THIS {This }
         | LBRACE expression RBRACE  {$2 }
         | classinstancecreationexpression {StmtExprExpr ($1) }
         | fieldaccess { $1}
         | methodinvocation {StmtExprExpr ($1) }		 

unaryexpressionnotplusminus : postfixexpression { $1 }
         | TILDE unaryexpression {Unary ("~",$2) }
         | EXCLMARK unaryexpression {Unary ("!",$2) }
         | castexpression{Jnull }

-- XOR == ^
         
exclusiveorexpression : andexpression { $1}
         | exclusiveorexpression XOR andexpression { Binary ("^",$1,$3)}

literal     : INTLITERAL { Integer $1}
         | BOOLLITERAL { Bool $1}
         | CHARLITERAL { Char $1}
         | STRINGLITERAL { String $1}
         | JNULL { Jnull}

castexpression     : LBRACE  primitivetype  RBRACE  unaryexpression { }
          | LBRACE  expression  RBRACE  unaryexpressionnotplusminus{ }

         -- AND == &
         
andexpression    : equalityexpression {$1 }
         | andexpression AND equalityexpression {Binary ("&",$1,$3) }

equalityexpression : relationalexpression {$1 }
         | equalityexpression EQUAL relationalexpression { Binary ("==",$1,$3)}
         | equalityexpression NOTEQUAL relationalexpression {Binary ("!=",$1,$3) }

relationalexpression : shiftexpression { $1 }
         | relationalexpression LESS shiftexpression { Binary ("<",$1,$3)}
         | relationalexpression GREATER shiftexpression {Binary (">",$1,$3) }
         | relationalexpression LESSEQUAL shiftexpression { Binary ("<=",$1,$3)}
         | relationalexpression GREATEREQUAL shiftexpression {Binary (">=",$1,$3) }
         | relationalexpression INSTANCEOF referencetype { Jnull}

shiftexpression     : additiveexpression { $1 }

additiveexpression : multiplicativeexpression { $1}
         | additiveexpression PLUS multiplicativeexpression { Binary("+",$1,$3)}
         | additiveexpression MINUS multiplicativeexpression {Binary("-",$1,$3) }

multiplicativeexpression : unaryexpression { $1}
         | multiplicativeexpression MUL unaryexpression { Binary("*",$1,$3)}
         | multiplicativeexpression DIV unaryexpression { Binary("/",$1,$3)}
         | multiplicativeexpression MOD unaryexpression { Binary("%",$1,$3)}


{


fst3 (a,b,c) = a 
snd3 (a,b,c) = b  
third3 (a,b,c) = c

{-
 className: used to find Constructors
 ms: Methods of Class
 checks if a Constructor is given and insert Default Constructor if not.
 Also sets  ReturnType and Name <init> of the Constructors

-}
insertConstr className ms = let ctrs = (checkConstrGuard className ms )
                            in (ctrs ++ ( filter (\m -> not (methodIsConstr className m)) ms))
                                
checkConstrGuard className ms = if (null ms) then [Method("void","<init>",[],Block[])]
                                  else (checkConstr className ms False)

checkConstr :: String -> [MethodDecl]  -> Bool -> [MethodDecl]
checkConstr className ((Method(typ,name,args,body)):ms) consFound
    | (name == className) && (null ms) = [Method ("void","<init>",args,body)]
    | name == className = [Method ("void","<init>",args,body)] ++ checkConstr className ms True
    | null ms = if (consFound) then [] else  [Method("void","<init>",[],Block[])]
    |otherwise = checkConstr className ms consFound
    
methodIsConstr className (Method(_,name,_,_)) = if (name == className) then True else False

-- seperate methods and fields
rule_classbodydeclarations arg1 arg2 = let f1 = fst arg1
                                           f2 = fst arg2    
                                           m1 = snd arg1
                                           m2 = snd arg2    
  
                                          in 
                                           (f1 ++ f2,m1 ++ m2)
checkForStringOrObject stringlit 
  |(stringlit == "String" || stringlit == "Object") = "java/lang/" ++ stringlit
  | otherwise = stringlit

-- create different assignments on different assignment operators  
rule_assignment left op right
  | (op == "=") = Assign(left,right)
  | (op == "+=") = Assign (left, Binary("+",left,right))
  | (op == "-=") = Assign (left, Binary("-",left,right))
  | (op == "/=") = Assign (left, Binary("/",left,right))
  | (op == "%=") = Assign (left, Binary("%",left,right))
  | (op == "*=") = Assign (left, Binary("*",left,right))
  | otherwise = Assign (Jnull,Jnull)

rule_postfixexpression arg = rule_lefthandside arg

rule_lefthandside (n:ns) 
  | null ns = LocalOrFieldVar (n)
  | otherwise = InstVar (fst (parseName(n:ns)),snd (parseName(n:ns) ))

{-
checks if the localvariabledeclarationstatement is just a 
simple declaration or an initialization and return
the LocalVarDecl and in second case also the Assignment
-}
splitInitialization (typ,(id,EmptyStmt)) =  [LocalVarDecl (typ,id)]
splitInitialization (typ,(id,stmt))      =  [LocalVarDecl (typ,id),stmt]

removeEmptyStmt :: [Stmt] -> [Stmt]
removeEmptyStmt (EmptyStmt:rest ) = []
removeEmptyStmt stmts = stmts

putStmtIntoBlock :: Stmt -> Stmt
putStmtIntoBlock (Block (y)) = Block (y)
putStmtIntoBlock stmt = ( Block [stmt] ) 

{-
used to parse singlename and qualifiedname.  Extracts the calling Expression (InstVar(...))
and the name  of the called Method.
-}
parseName :: [String] -> (Expr,String)
parseName (n:ns) 
  | null ns = (This,n)
  | otherwise = (getInstVarExpr (reverse (init (n:ns))) , last ns)

getInstVarExpr (n:ns) 
 | null ns = LocalOrFieldVar(n)
 | otherwise = InstVar (getInstVarExpr ns,n)

--
--  I/O and test functions
--
    
parseError :: [Token] -> a
parseError _ = error "Parse error"

parser :: String -> [Class]
parser = parse.alexScanTokens

readEingabe = do
  s <- readFile "Eingabe.java"
  print (parser s)
  
main = do 
  s <- readFile "Eingabe.java"
  print (parser s)

} 
