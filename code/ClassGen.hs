module ClassGen(generateClassFileStruct) where

import Libraries.Data.ClassFormat
import ConstPoolLookup

getClassLookup:: ConstPoolLookup -> ClassRefLookup
getClassLookup (ConstPoolLookup(_,_,classLookup,_,_)) = classLookup


generateClassFileStruct:: CP_Infos -> String ->Field_Infos -> Method_Infos -> ClassFile
generateClassFileStruct cp_infos className field_infos method_infos =
 let constPoolLookup = createConstPoolLookup cp_infos
     classLookup = getClassLookup constPoolLookup
 in
 ClassFile {
 magic = Magic,
 minver = MinorVersion {numMinVer = 0},
 maxver = MajorVersion {numMaxVer = 47},
 count_cp = ((length cp_infos)+1),
 array_cp = cp_infos,
 acfg = AccessFlags [32],
 this = ThisClass {index_th = (classLookup className)},
 super = SuperClass {index_sp = (classLookup "java/lang/Object")},
 count_interfaces = 0,
 array_interfaces = [],
 count_fields = (length field_infos),
 array_fields = field_infos,
 count_methods = (length method_infos),
 array_methods = method_infos,
 count_attributes = 0,
 array_attributes = []
}