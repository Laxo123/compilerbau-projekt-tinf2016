module ConstPoolLookup(ConstPoolLookup(ConstPoolLookup),Utf8InfoLookup,MethodRefLookup,ClassRefLookup,FieldRefLookup,ValueConstantLookup,createConstPoolLookup,createUtf8InfoLookup,createClassRefLookup,createFieldRefLookup,createMethodRefLookup,createValueConstantLookup) where

import qualified Data.Map as Map
import Data.Maybe
import Data.Tuple
import Libraries.Data.ClassFormat
import Abstract_syntax.AbsSyn


type Utf8InfoLookup = (String->Int)
type MethodRefLookup = (Type->String->Int)
type ClassRefLookup = (Type->Int)
type FieldRefLookup = (Type->String->Int)
type ValueConstantLookup = (Expr->Int)

--Definition of the data struct that contains the lookup functions
data ConstPoolLookup = ConstPoolLookup(Utf8InfoLookup,MethodRefLookup,ClassRefLookup,FieldRefLookup,ValueConstantLookup)

--function for creation the lookup functions struct for a given constant pool(CP_Infos)
createConstPoolLookup:: CP_Infos -> ConstPoolLookup
createConstPoolLookup infos =
 let utf8Lookup = createUtf8InfoLookup infos
     methodRefLookup= createMethodRefLookup infos
     classRefLookup = createClassRefLookup infos
     fieldRefLookup = createFieldRefLookup infos
     valueConstLookup = createValueConstantLookup infos
 in
 ConstPoolLookup(utf8Lookup,methodRefLookup,classRefLookup,fieldRefLookup,valueConstLookup)

--map lookup with inverted parameter order(so we can curry the the lookup on a specific map)
customLookup:: (Map.Map String Int) -> String -> Int
customLookup map key = (fromJust (Map.lookup key map))

customTwoKeyLookup:: (Map.Map (String,String) Int) -> String -> String -> Int
customTwoKeyLookup map key1 key2 = (fromJust(Map.lookup (key1,key2) map))

createInvertedMap:: Ord a => Ord b => (Map.Map a b) -> (Map.Map b a)
createInvertedMap inputMap = Map.fromList (map swap (Map.toList inputMap))


--Uft8_Infos
createUtf8InfoLookup:: CP_Infos -> Utf8InfoLookup
createUtf8InfoLookup infos = customLookup(createUtf8InfoLookupInternal infos 1 Map.empty)

createUtf8InfoLookupInternal:: CP_Infos -> Int ->(Map.Map String Int) -> (Map.Map String Int)
createUtf8InfoLookupInternal [] counter map = map
createUtf8InfoLookupInternal ((Utf8_Info tag length content description):infos)  counter map= createUtf8InfoLookupInternal infos (counter+1) (Map.insert content counter map)
createUtf8InfoLookupInternal (_:infos)  counter map= createUtf8InfoLookupInternal infos (counter+1) map

--NameAndType_Info
createNameAndTypeLookupMap:: CP_Infos -> (Map.Map String Int)
createNameAndTypeLookupMap infos =
 let invertedUtf8Map = createInvertedMap (createUtf8InfoLookupInternal infos 1 Map.empty)
 in
 createNameAndTypeLookupMapInternal invertedUtf8Map infos 1 Map.empty

createNameAndTypeLookupMapInternal:: (Map.Map Int String) -> CP_Infos -> Int ->(Map.Map String Int) -> (Map.Map String Int)
createNameAndTypeLookupMapInternal _ [] _ map = map
createNameAndTypeLookupMapInternal invertedUtf8Map ((NameAndType_Info _ nameIndex _ _):infos) counter map = createNameAndTypeLookupMapInternal invertedUtf8Map infos (counter+1) (Map.insert (fromJust (Map.lookup nameIndex invertedUtf8Map)) counter map)
createNameAndTypeLookupMapInternal invertedUtf8Map (_:infos) counter map = createNameAndTypeLookupMapInternal invertedUtf8Map infos (counter+1) map

--Class_Info
createClassRefLookup:: CP_Infos -> ClassRefLookup
createClassRefLookup infos =
 let invertedUtf8Map = createInvertedMap (createUtf8InfoLookupInternal infos 1 Map.empty)
 in
 customLookup( createClassRefLookupInternal invertedUtf8Map infos 1 Map.empty)

createClassRefLookupInternal:: (Map.Map Int String)-> CP_Infos -> Int -> (Map.Map String Int) -> (Map.Map String Int)
createClassRefLookupInternal invertedUtf8Map [] counter map = map
createClassRefLookupInternal invertedUtf8Map ((Class_Info tag nameIndex _):infos) counter map = createClassRefLookupInternal  invertedUtf8Map infos (counter+1) (Map.insert (fromJust (Map.lookup nameIndex invertedUtf8Map)) counter map)
createClassRefLookupInternal invertedUtf8Map (_:infos) counter map = createClassRefLookupInternal  invertedUtf8Map infos (counter+1) map


--FieldRefLookup
createFieldRefLookup:: CP_Infos -> FieldRefLookup
createFieldRefLookup infos=
  let invertedNameAndTypeLookup = createInvertedMap (createNameAndTypeLookupMap infos)
      invertedUtf8Map = createInvertedMap (createUtf8InfoLookupInternal infos 1 Map.empty)
      invertedClassRefLookup = createInvertedMap (createClassRefLookupInternal invertedUtf8Map infos 1 Map.empty)
  in
 customTwoKeyLookup (createFieldRefLookupInternal invertedNameAndTypeLookup invertedClassRefLookup infos 1 Map.empty)

createFieldRefLookupInternal::  (Map.Map Int String) -> (Map.Map Int String) -> CP_Infos -> Int ->  (Map.Map (Type,String) Int) -> (Map.Map (Type,String) Int)
createFieldRefLookupInternal _ _ [] _ map = map
createFieldRefLookupInternal indexNameLookup indexClassRefLookup ((FieldRef_Info _ classIndex nameIndex _):infos) counter map = createFieldRefLookupInternal indexNameLookup indexClassRefLookup infos (counter+1) (Map.insert ((fromJust (Map.lookup classIndex indexClassRefLookup)),(fromJust (Map.lookup nameIndex indexNameLookup))) counter map)
createFieldRefLookupInternal indexNameLookup indexClassRefLookup (_:infos) counter map = createFieldRefLookupInternal indexNameLookup indexClassRefLookup infos (counter+1) map


--MethodRefLookup
createMethodRefLookup:: CP_Infos -> MethodRefLookup
createMethodRefLookup infos=
 let invertedNameAndTypeLookup = createInvertedMap (createNameAndTypeLookupMap infos)
     invertedUtf8Map = createInvertedMap (createUtf8InfoLookupInternal infos 1 Map.empty)
     invertedClassRefLookup = createInvertedMap (createClassRefLookupInternal invertedUtf8Map infos 1 Map.empty)
  in
 customTwoKeyLookup ( createMethodRefLookupInternal invertedNameAndTypeLookup invertedClassRefLookup infos 1 Map.empty)

createMethodRefLookupInternal::  (Map.Map Int String) -> (Map.Map Int String) -> CP_Infos -> Int ->  (Map.Map (String,String) Int) -> (Map.Map (String,String) Int)
createMethodRefLookupInternal _ _ [] _ map = map
createMethodRefLookupInternal indexNameLookup indexClassRefLookup ((MethodRef_Info _ classIndex nameIndex _):infos) counter map = createMethodRefLookupInternal indexNameLookup indexClassRefLookup infos (counter+1) (Map.insert ((fromJust (Map.lookup classIndex indexClassRefLookup)),(fromJust (Map.lookup nameIndex indexNameLookup))) counter map)
createMethodRefLookupInternal indexNameLookup indexClassRefLookup (_:infos) counter map = createMethodRefLookupInternal indexNameLookup indexClassRefLookup infos (counter+1) map

--ValueConstantLookup
createValueConstantLookup:: CP_Infos -> ValueConstantLookup
createValueConstantLookup infos =
 let integerMap = createIntegerConstantMap infos 1 Map.empty
     utf8LookupMap = (createUtf8InfoLookupInternal infos 1 Map.empty)
     invertedUtf8Map = Map.fromList (map swap (Map.toList utf8LookupMap))
     stringMap = createStringConstantMap infos invertedUtf8Map 1 Map.empty
 in
 constValueLookupInternal integerMap stringMap

constValueLookupInternal:: (Map.Map Int Int) -> (Map.Map String Int) -> Expr -> Int
constValueLookupInternal intMap stringMap (Integer(value)) = fromJust(Map.lookup (fromIntegral value) intMap)
constValueLookupInternal intMap stringMap (String(value)) = fromJust(Map.lookup value stringMap)

createIntegerConstantMap:: CP_Infos -> Int -> (Map.Map Int Int) -> (Map.Map Int Int)
createIntegerConstantMap [] counter map = map
createIntegerConstantMap ((Integer_Info tag value _):infos) counter map = createIntegerConstantMap infos (counter+1) (Map.insert value counter map)
createIntegerConstantMap (_:infos) counter map = createIntegerConstantMap infos (counter+1) map

createStringConstantMap:: CP_Infos -> (Map.Map Int String) -> Int -> (Map.Map String Int) ->(Map.Map String Int)
createStringConstantMap [] _ _ map = map
createStringConstantMap ((String_Info _ valueIndex _):infos) valueIndexLookup counter map = createStringConstantMap infos valueIndexLookup (counter+1) (Map.insert (fromJust(Map.lookup valueIndex valueIndexLookup)) counter map)
createStringConstantMap (_:infos) valueIndexLookup counter map = createStringConstantMap infos valueIndexLookup (counter+1) map