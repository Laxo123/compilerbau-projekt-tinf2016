module CompilerMain(generateClassCode) where

import Abstract_syntax.AbsSyn
import CodeGen
import ClassGen
import MethodGen
import ConstPoolLookup
import Libraries.Data.ClassFormat
import FieldGen
import JavaParser
import TypedAbsSyntax
import ConstPool
import Libraries.BinaryClass

generateClassCode:: CP_Infos -> Class -> (String,ClassFile)
generateClassCode infos (Class(name, fields, methods)) =
 let constPoolLookup = createConstPoolLookup infos
     methodInfos = generateMethods constPoolLookup name methods
     fieldInfos = generateFields constPoolLookup fields
 in
 (name,(generateClassFileStruct infos  name fieldInfos methodInfos))

writeClassToDisc:: (String,ClassFile) -> IO ()
writeClassToDisc nameAndFile= encodeClassFile ((fst nameAndFile)++".class") (snd nameAndFile)


main = do
    putStrLn "Please enter the path to you java file"
    fileName <- getLine
    s <- readFile fileName
    putStrLn "Abstract Syntax"
    print (parser s)
    putStrLn "Typed Abstract Syntax"
    print (javatypecheck (parser s))
    putStrLn "Constant Pool"
    print (fillConstPoolStart (javatypecheck (parser s)))
    foldr (>>) (return()) (map writeClassToDisc  (let absSyn= parser s
                                                      typedSyn=javatypecheck absSyn
                                                      constPool= fillConstPoolStart typedSyn
                                                  in
                                                  map (generateClassCode constPool) typedSyn))

