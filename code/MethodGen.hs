module MethodGen(generateMethods) where

import Libraries.Data.ClassFormat
import Abstract_syntax.AbsSyn
import qualified Data.Map as Map
import Data.Maybe
import Data.Char
import CodeGen
import ConstPoolLookup
import Descriptor



generateMethods:: ConstPoolLookup ->Type ->[MethodDecl] ->Method_Infos
generateMethods constPoolLookup typeName  methods = map ((generateSingleMethod constPoolLookup typeName)) methods


isReturn:: ByteCodeMnemonic -> Bool
isReturn RETURN = True
isReturn x = False

generateSingleMethod:: ConstPoolLookup -> Type -> MethodDecl  -> Method_Info
generateSingleMethod (ConstPoolLookup(utf8Lookup,methodRefLookup,typeLookup,fieldLookup,constantLookup))   typeName (Method(returnTypeName, methodName,args, statement))  =
 let vars=(args  ++(getLocalVarDecls statement))
     varMap = (numberVars vars)
     lookupFuncs = CodeGenLookups((customLookup varMap),methodRefLookup,fieldLookup,typeLookup,constantLookup)
     callBaseMnemonic = (if methodName=="<init>" then [ALOAD_0,INVOKESPECIAL(methodRefLookup "java/lang/Object" "<init>")]  else [])
     codeMnemonic =  callBaseMnemonic++(generateStmtCode lookupFuncs )statement
     codeMnemonicWithReturn = (if returnTypeName == "void" && ((null codeMnemonic) || not((isReturn (last codeMnemonic)))) then codeMnemonic ++ [RETURN] else codeMnemonic)
     byteCode =  (concat(map (encodeByteCodeMnemonic) codeMnemonicWithReturn))
 in
    Method_Info
    { af_mi          = (AccessFlags[acc_Public])
    , index_name_mi  = (utf8Lookup methodName)                                                                             -- name_index
    , index_descr_mi = (utf8Lookup (getMethodDescriptor (Method(returnTypeName, methodName,args, statement))))                             -- descriptor_index
    , tam_mi         = 1                                                                                                                 -- attributes_count
    , array_attr_mi  = [
    AttributeCode
                { index_name_attr           = (utf8Lookup "Code")               -- attribute_name_index
                , tam_len_attr              = (2+2+4+(length byteCode)+2+2)     -- attribute_length
                , len_stack_attr            = 1024                              -- max_stack
                , len_local_attr            = ((length vars) +1)                    -- max_local
                , tam_code_attr             = (length byteCode)                 -- code_length
                , array_code_attr           = byteCode                          -- code como array de bytes
                , tam_ex_attr               = 0                                 -- exceptions_length
                , array_ex_attr             = []                                -- no usamos
                , tam_atrr_attr             = 0                                 -- attributes_count
                , array_attr_attr           =[]
                }
                ]
                }







--map lookup with inverted parameter order(so we can curry the the lookup on a specific map)
customLookup:: (Map.Map String Int) -> String -> Int
customLookup map key = (fromJust (Map.lookup key map))


--Takes a statement and returns all in the code that is represented by the statement(the statement can be the root node of a whole syntax tree)
getLocalVarDecls:: Stmt -> [(String, String)]
--Empty block or recursion end.
getLocalVarDecls (Block ([]))= []
getLocalVarDecls (Block (statement:statements)) =  getLocalVarDecls(statement) ++  getLocalVarDecls(Block(statements))
getLocalVarDecls (Return(_)) = []
getLocalVarDecls (While(_ , statement )) = getLocalVarDecls(statement)
getLocalVarDecls (LocalVarDecl(typeName, varName))  = [(typeName, varName)]
getLocalVarDecls (If(_, statement1 , Just statement2)) = getLocalVarDecls(statement1) ++ getLocalVarDecls(statement2)
getLocalVarDecls (If(_, statement1 , Nothing)) = getLocalVarDecls(statement1)
getLocalVarDecls (StmtExprStmt(_)) =[]
getLocalVarDecls (TypedStmt(statement, typeName))  = getLocalVarDecls(statement)

--Numbers a list of variable names and returns a map that can be used to get the number(the local variable index) from the variable or parameter name.
numberVars:: [(typeName, String)] -> Map.Map String Int
numberVars decls =   snd (numberVarsInternal decls Map.empty)

--Internal recursive implementation of the numberVars function.
numberVarsInternal:: [(typeName, String)] -> Map.Map String Int -> ([(typeName, String)], Map.Map String Int)
numberVarsInternal [] map = ([],map)
numberVarsInternal ((_,name):decls) map =  let lookupResult =Map.lookup name map
                                           in
                                           if isJust lookupResult then (numberVarsInternal decls map)
                                           else (numberVarsInternal decls (Map.insert  name ((length (Map.keys map))+1) map))