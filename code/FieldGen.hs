module FieldGen(generateFields) where
import Descriptor
import ConstPoolLookup
import Libraries.Data.ClassFormat
import Abstract_syntax.AbsSyn

generateFields:: ConstPoolLookup -> [FieldDecl] -> Field_Infos
generateFields lookup fields = map (generateSingleField lookup) fields
generateFields lookup [] = []


generateSingleField:: ConstPoolLookup -> FieldDecl -> Field_Info
generateSingleField (ConstPoolLookup(utf8Lookup,methodRefLookup,typeLookup,fieldLookup,constantLookup))  (FieldDecl(typeName, fieldName)) =
 Field_Info
                        { af_fi          = (AccessFlags[acc_Public])
                        , index_name_fi  = (utf8Lookup fieldName)                            -- name_index
                        , index_descr_fi = (utf8Lookup (getFieldDescriptor typeName))       -- descriptor_index
                        , tam_fi         = 0                                                -- count_attributte
                        , array_attr_fi  = []
                        }
