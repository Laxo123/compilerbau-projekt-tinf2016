module CodeGen where

import Libraries.Data.ClassFormat
import Abstract_syntax.AbsSyn
import qualified Data.Map as Map
import Data.Maybe
import Data.Char
import Data.Bits

--Definition der Java Bytecode Befehle
data ByteCodeMnemonic = NOP |
                        ACONST_NULL |
                        ICONST_M1 |
                        ICONST_0 |
                        ICONST_1 |
                        ICONST_2 |
                        ICONST_3 |
                        ICONST_4 |
                        ICONST_5 |
                        LCONST_0 |
                        LCONST_1 |
                        FCONST_0 |
                        FCONST_1 |
                        FCONST_2 |
                        DCONST_0 |
                        DCONST_1 |
                        BIPUSH(Int)|
                        SIPUSH(Int,Int)|
                        LDC(Int)|
                        LDC_W(Int,Int)|
                        LDC2_W(Int,Int)|
                        ILOAD(Int)|
                        LLOAD(Int)|
                        FLOAD(Int)|
                        DLOAD(Int)|
                        ALOAD(Int)|
                        ILOAD_0 |
                        ILOAD_1 |
                        ILOAD_2 |
                        ILOAD_3 |
                        LLOAD_0 |
                        LLOAD_1 |
                        LLOAD_2 |
                        LLOAD_3 |
                        FLOAD_0 |
                        FLOAD_1 |
                        FLOAD_2 |
                        FLOAD_3 |
                        DLOAD_0 |
                        DLOAD_1 |
                        DLOAD_2 |
                        DLOAD_3 |
                        ALOAD_0 |
                        ALOAD_1 |
                        ALOAD_2 |
                        ALOAD_3 |
                        IALOAD |
                        LALOAD |
                        FALOAD |
                        DALOAD |
                        AALOAD |
                        BALOAD |
                        CALOAD |
                        SALOAD |
                        ISTORE(Int)|
                        LSTORE(Int)|
                        FSTORE(Int)|
                        DSTORE(Int)|
                        ASTORE(Int)|
                        ISTORE_0 |
                        ISTORE_1 |
                        ISTORE_2 |
                        ISTORE_3 |
                        LSTORE_0 |
                        LSTORE_1 |
                        LSTORE_2 |
                        LSTORE_3 |
                        FSTORE_0 |
                        FSTORE_1 |
                        FSTORE_2 |
                        FSTORE_3 |
                        DSTORE_0 |
                        DSTORE_1 |
                        DSTORE_2 |
                        DSTORE_3 |
                        ASTORE_0 |
                        ASTORE_1 |
                        ASTORE_2 |
                        ASTORE_3 |
                        IASTORE |
                        LASTORE |
                        FASTORE |
                        DASTORE |
                        AASTORE |
                        BASTORE |
                        CASTORE |
                        SASTORE |
                        POP |
                        POP2 |
                        DUP |
                        DUP_X1 |
                        DUP_X2 |
                        DUP2 |
                        DUP2_X1 |
                        DUP2_X2 |
                        SWAP |
                        IADD |
                        LADD |
                        FADD |
                        DADD |
                        ISUB |
                        LSUB |
                        FSUB |
                        DSUB |
                        IMUL |
                        LMUL |
                        FMUL |
                        DMUL |
                        IDIV |
                        LDIV |
                        FDIV |
                        DDIV |
                        IREM |
                        LREM |
                        FREM |
                        DREM |
                        INEG |
                        LNEG |
                        FNEG |
                        DNEG |
                        ISHL |
                        LSHL |
                        ISHR |
                        LSHR |
                        IUSHR |
                        LUSHR |
                        IAND |
                        LAND |
                        IOR |
                        LOR |
                        IXOR |
                        LXOR |
                        IINC(Int,Int)|
                        I2L |
                        I2F |
                        I2D |
                        L2I |
                        L2F |
                        L2D |
                        F2I |
                        F2L |
                        F2D |
                        D2I |
                        D2L |
                        D2F |
                        I2B |
                        I2C |
                        I2S |
                        LCMP |
                        FCMPL |
                        FCMPG |
                        DCMPL |
                        DCMPG |
                        IFEQ(Int) |
                        IFNE(Int) |
                        IFLT(Int) |
                        IFGE(Int) |
                        IFGT(Int) |
                        IFLE(Int) |
                        IF_ICMPEQ(Int) |
                        IF_ICMPNE(Int) |
                        IF_ICMPLT(Int) |
                        IF_ICMPGE(Int) |
                        IF_ICMPGT(Int) |
                        IF_ICMPLE(Int) |
                        IF_ACMPEQ(Int) |
                        IF_ACMPNE(Int) |
                        GOTO(Int) |
                        JSR(Int) |
                        RET(Int)|
                        TABLESWITCH |
                        LOOKUPSWITCH |
                        IRETURN |
                        LRETURN |
                        FRETURN |
                        DRETURN |
                        ARETURN |
                        RETURN |
                        GETSTATIC(Int,Int)|
                        PUTSTATIC(Int,Int)|
                        GETFIELD(Int)|
                        PUTFIELD(Int)|
                        INVOKEVIRTUAL(Int)|
                        INVOKESPECIAL(Int)|
                        INVOKESTATIC(Int)|
                        INVOKEINTERFACE(Int,Int,Int,Int)|
                        INVOKEDYNAMIC(Int,Int,Int,Int)|
                        NEW(Int)|
                        NEWARRAY(Int)|
                        ANEWARRAY(Int,Int)|
                        ARRAYLENGTH |
                        ATHROW |
                        CHECKCAST(Int,Int)|
                        INSTANCEOF(Int,Int)|
                        MONITORENTER |
                        MONITOREXIT |
                        WIDE |
                        MULTIANEWARRAY(Int,Int,Int)|
                        IFNULL(Int) |
                        IFNONNULL(Int) |
                        GOTO_W(Int,Int,Int,Int)|
                        JSR_W(Int,Int,Int,Int)|
                        BREAKPOINT |
                        IMPDEP1 |
                        IMPDEP2 deriving (Show)


encodeByteCodeMnemonic:: ByteCodeMnemonic -> [Int]
encodeByteCodeMnemonic NOP = [0x0]
encodeByteCodeMnemonic ACONST_NULL= [0x1]
encodeByteCodeMnemonic ICONST_M1= [0x2]
encodeByteCodeMnemonic ICONST_0= [0x3]
encodeByteCodeMnemonic ICONST_1= [0x4]
encodeByteCodeMnemonic ICONST_2= [0x5]
encodeByteCodeMnemonic ICONST_3= [0x6]
encodeByteCodeMnemonic ICONST_4= [0x7]
encodeByteCodeMnemonic ICONST_5= [0x8]
encodeByteCodeMnemonic LCONST_0= [0x9]
encodeByteCodeMnemonic LCONST_1= [0xA]
encodeByteCodeMnemonic FCONST_0= [0xB]
encodeByteCodeMnemonic FCONST_1= [0xC]
encodeByteCodeMnemonic FCONST_2= [0xD]
encodeByteCodeMnemonic DCONST_0= [0xE]
encodeByteCodeMnemonic DCONST_1= [0xF]
encodeByteCodeMnemonic (BIPUSH(a)) = [0x10,a]
encodeByteCodeMnemonic (SIPUSH(a , b))= [0x11,a,b]
encodeByteCodeMnemonic (LDC(a))= [0x12,a]
encodeByteCodeMnemonic (LDC_W(a , b))= [0x13,a,b]
encodeByteCodeMnemonic (LDC2_W(a , b))= [0x14,a,b]
encodeByteCodeMnemonic (ILOAD(a))= [0x15,a]
encodeByteCodeMnemonic (LLOAD(a))= [0x16,a]
encodeByteCodeMnemonic (FLOAD(a))= [0x17,a]
encodeByteCodeMnemonic (DLOAD(a))= [0x18,a]
encodeByteCodeMnemonic (ALOAD(a))= [0x19,a]
encodeByteCodeMnemonic ILOAD_0= [0x1A]
encodeByteCodeMnemonic ILOAD_1= [0x1B]
encodeByteCodeMnemonic ILOAD_2= [0x1C]
encodeByteCodeMnemonic ILOAD_3= [0x1D]
encodeByteCodeMnemonic LLOAD_0= [0x1E]
encodeByteCodeMnemonic LLOAD_1= [0x1F]
encodeByteCodeMnemonic LLOAD_2= [0x20]
encodeByteCodeMnemonic LLOAD_3= [0x21]
encodeByteCodeMnemonic FLOAD_0= [0x22]
encodeByteCodeMnemonic FLOAD_1= [0x23]
encodeByteCodeMnemonic FLOAD_2= [0x24]
encodeByteCodeMnemonic FLOAD_3= [0x25]
encodeByteCodeMnemonic DLOAD_0= [0x26]
encodeByteCodeMnemonic DLOAD_1= [0x27]
encodeByteCodeMnemonic DLOAD_2= [0x28]
encodeByteCodeMnemonic DLOAD_3= [0x29]
encodeByteCodeMnemonic ALOAD_0= [0x2A]
encodeByteCodeMnemonic ALOAD_1= [0x2B]
encodeByteCodeMnemonic ALOAD_2= [0x2C]
encodeByteCodeMnemonic ALOAD_3= [0x2D]
encodeByteCodeMnemonic IALOAD= [0x2E]
encodeByteCodeMnemonic LALOAD= [0x2F]
encodeByteCodeMnemonic FALOAD= [0x30]
encodeByteCodeMnemonic DALOAD= [0x31]
encodeByteCodeMnemonic AALOAD= [0x32]
encodeByteCodeMnemonic BALOAD= [0x33]
encodeByteCodeMnemonic CALOAD= [0x34]
encodeByteCodeMnemonic SALOAD= [0x35]
encodeByteCodeMnemonic (ISTORE(a))= [0x36,a]
encodeByteCodeMnemonic (LSTORE(a))= [0x37,a]
encodeByteCodeMnemonic (FSTORE(a))= [0x38,a]
encodeByteCodeMnemonic (DSTORE(a))= [0x39,a]
encodeByteCodeMnemonic (ASTORE(a))= [0x3A,a]
encodeByteCodeMnemonic ISTORE_0= [0x3B]
encodeByteCodeMnemonic ISTORE_1= [0x3C]
encodeByteCodeMnemonic ISTORE_2= [0x3D]
encodeByteCodeMnemonic ISTORE_3= [0x3E]
encodeByteCodeMnemonic LSTORE_0= [0x3F]
encodeByteCodeMnemonic LSTORE_1= [0x40]
encodeByteCodeMnemonic LSTORE_2= [0x41]
encodeByteCodeMnemonic LSTORE_3= [0x42]
encodeByteCodeMnemonic FSTORE_0= [0x43]
encodeByteCodeMnemonic FSTORE_1= [0x44]
encodeByteCodeMnemonic FSTORE_2= [0x45]
encodeByteCodeMnemonic FSTORE_3= [0x46]
encodeByteCodeMnemonic DSTORE_0= [0x47]
encodeByteCodeMnemonic DSTORE_1= [0x48]
encodeByteCodeMnemonic DSTORE_2= [0x49]
encodeByteCodeMnemonic DSTORE_3= [0x4A]
encodeByteCodeMnemonic ASTORE_0= [0x4B]
encodeByteCodeMnemonic ASTORE_1= [0x4C]
encodeByteCodeMnemonic ASTORE_2= [0x4D]
encodeByteCodeMnemonic ASTORE_3= [0x4E]
encodeByteCodeMnemonic IASTORE= [0x4F]
encodeByteCodeMnemonic LASTORE= [0x50]
encodeByteCodeMnemonic FASTORE= [0x51]
encodeByteCodeMnemonic DASTORE= [0x52]
encodeByteCodeMnemonic AASTORE= [0x53]
encodeByteCodeMnemonic BASTORE= [0x54]
encodeByteCodeMnemonic CASTORE= [0x55]
encodeByteCodeMnemonic SASTORE= [0x56]
encodeByteCodeMnemonic POP= [0x57]
encodeByteCodeMnemonic POP2= [0x58]
encodeByteCodeMnemonic DUP= [0x59]
encodeByteCodeMnemonic DUP_X1= [0x5A]
encodeByteCodeMnemonic DUP_X2= [0x5B]
encodeByteCodeMnemonic DUP2= [0x5C]
encodeByteCodeMnemonic DUP2_X1= [0x5D]
encodeByteCodeMnemonic DUP2_X2= [0x5E]
encodeByteCodeMnemonic SWAP= [0x5F]
encodeByteCodeMnemonic IADD= [0x60]
encodeByteCodeMnemonic LADD= [0x61]
encodeByteCodeMnemonic FADD= [0x62]
encodeByteCodeMnemonic DADD= [0x63]
encodeByteCodeMnemonic ISUB= [0x64]
encodeByteCodeMnemonic LSUB= [0x65]
encodeByteCodeMnemonic FSUB= [0x66]
encodeByteCodeMnemonic DSUB= [0x67]
encodeByteCodeMnemonic IMUL= [0x68]
encodeByteCodeMnemonic LMUL= [0x69]
encodeByteCodeMnemonic FMUL= [0x6A]
encodeByteCodeMnemonic DMUL= [0x6B]
encodeByteCodeMnemonic IDIV= [0x6C]
encodeByteCodeMnemonic LDIV= [0x6D]
encodeByteCodeMnemonic FDIV= [0x6E]
encodeByteCodeMnemonic DDIV= [0x6F]
encodeByteCodeMnemonic IREM= [0x70]
encodeByteCodeMnemonic LREM= [0x71]
encodeByteCodeMnemonic FREM= [0x72]
encodeByteCodeMnemonic DREM= [0x73]
encodeByteCodeMnemonic INEG= [0x74]
encodeByteCodeMnemonic LNEG= [0x75]
encodeByteCodeMnemonic FNEG= [0x76]
encodeByteCodeMnemonic DNEG= [0x77]
encodeByteCodeMnemonic ISHL= [0x78]
encodeByteCodeMnemonic LSHL= [0x79]
encodeByteCodeMnemonic ISHR= [0x7A]
encodeByteCodeMnemonic LSHR= [0x7B]
encodeByteCodeMnemonic IUSHR= [0x7C]
encodeByteCodeMnemonic LUSHR= [0x7D]
encodeByteCodeMnemonic IAND= [0x7E]
encodeByteCodeMnemonic LAND= [0x7F]
encodeByteCodeMnemonic IOR= [0x80]
encodeByteCodeMnemonic LOR= [0x81]
encodeByteCodeMnemonic IXOR= [0x82]
encodeByteCodeMnemonic LXOR= [0x83]
encodeByteCodeMnemonic (IINC(a , b))= [0x84,a,b]
encodeByteCodeMnemonic I2L= [0x85]
encodeByteCodeMnemonic I2F= [0x86]
encodeByteCodeMnemonic I2D= [0x87]
encodeByteCodeMnemonic L2I= [0x88]
encodeByteCodeMnemonic L2F= [0x89]
encodeByteCodeMnemonic L2D= [0x8A]
encodeByteCodeMnemonic F2I= [0x8B]
encodeByteCodeMnemonic F2L= [0x8C]
encodeByteCodeMnemonic F2D= [0x8D]
encodeByteCodeMnemonic D2I= [0x8E]
encodeByteCodeMnemonic D2L= [0x8F]
encodeByteCodeMnemonic D2F= [0x90]
encodeByteCodeMnemonic I2B= [0x91]
encodeByteCodeMnemonic I2C= [0x92]
encodeByteCodeMnemonic I2S= [0x93]
encodeByteCodeMnemonic LCMP= [0x94]
encodeByteCodeMnemonic FCMPL= [0x95]
encodeByteCodeMnemonic FCMPG= [0x96]
encodeByteCodeMnemonic DCMPL= [0x97]
encodeByteCodeMnemonic DCMPG= [0x98]
encodeByteCodeMnemonic (IFEQ(x))= [0x99,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFNE(x))= [0x9A,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFLT(x))= [0x9B,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFGE(x))= [0x9C,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFGT(x))= [0x9D,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFLE(x))= [0x9E,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPEQ(x))= [0x9F,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPNE(x))= [0xA0,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPLT(x))= [0xA1,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPGE(x))= [0xA2,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPGT(x))= [0xA3,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ICMPLE(x))= [0xA4,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ACMPEQ(x))= [0xA5,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IF_ACMPNE(x))= [0xA6,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (GOTO(x))= if x >= 0 then [0xA7,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
                                  else 
                                  let complementOffset = (complement (x*(-1)))+1
                                  in
                                  [0xA7,(shift((.&.) complementOffset (fromInteger(0xFF00) :: Int)) (-8))  ,(.&.) complementOffset(fromInteger(255) :: Int)]
                                   
encodeByteCodeMnemonic (JSR(x))= [0xA8,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (RET(a))= [0xA9,a]
encodeByteCodeMnemonic TABLESWITCH= [0xAA]
encodeByteCodeMnemonic LOOKUPSWITCH= [0xAB]
encodeByteCodeMnemonic IRETURN= [0xAC]
encodeByteCodeMnemonic LRETURN= [0xAD]
encodeByteCodeMnemonic FRETURN= [0xAE]
encodeByteCodeMnemonic DRETURN= [0xAF]
encodeByteCodeMnemonic ARETURN= [0xB0]
encodeByteCodeMnemonic RETURN= [0xB1]
encodeByteCodeMnemonic (GETSTATIC(a , b))= [0xB2,a,b]
encodeByteCodeMnemonic (PUTSTATIC(a , b))= [0xB3,a,b]
encodeByteCodeMnemonic (GETFIELD(x))= [0xB4,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (PUTFIELD(x))= [0xB5,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (INVOKEVIRTUAL(x))= [0xB6,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (INVOKESPECIAL(x))= [0xB7,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (INVOKESTATIC(x))= [0xB8,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (INVOKEINTERFACE(a , b , c , d))= [0xB9,a,b,c,d]
encodeByteCodeMnemonic (INVOKEDYNAMIC(a , b , c , d))= [0xBA,a,b,c,d]
encodeByteCodeMnemonic (NEW(x))= [0xBB,x  `quot`  (fromInteger(255):: Int), x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (NEWARRAY(a))= [0xBC,a]
encodeByteCodeMnemonic (ANEWARRAY(a , b))= [0xBD,a,b]
encodeByteCodeMnemonic ARRAYLENGTH= [0xBE]
encodeByteCodeMnemonic ATHROW= [0xBF]
encodeByteCodeMnemonic (CHECKCAST(a , b))= [0xC0,a,b]
encodeByteCodeMnemonic (INSTANCEOF(a , b))= [0xC1,a,b]
encodeByteCodeMnemonic MONITORENTER= [0xC2]
encodeByteCodeMnemonic MONITOREXIT= [0xC3]
encodeByteCodeMnemonic WIDE= [0xC4]
encodeByteCodeMnemonic (MULTIANEWARRAY(a , b , c))= [0xC5,a,b,c]
encodeByteCodeMnemonic (IFNULL(x))= [0xC6,x  `quot`  (fromInteger(255) :: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (IFNONNULL(x))= [0xC7,x  `quot`  (fromInteger(255):: Int),x`mod`(fromInteger(255) :: Int)]
encodeByteCodeMnemonic (GOTO_W(a , b , c , d))= [0xC8,a,b,c,d]
encodeByteCodeMnemonic (JSR_W(a , b , c , d))= [0xC9,a,b,c,d]
encodeByteCodeMnemonic BREAKPOINT= [0xCA]
encodeByteCodeMnemonic IMPDEP1= [0xFE]
encodeByteCodeMnemonic IMPDEP2= [0xFF]

--Berechnet die Länge einer Liste von Bytecode Befehlen
codeLength:: [ByteCodeMnemonic] -> Int
codeLength [] = 0
codeLength (x : xs) = (length (encodeByteCodeMnemonic x)) + (codeLength xs)

-- var,method,field,type,constant
data CodeGenLookups = CodeGenLookups(String->Int,Type->String->Int,Type->String->Int, Type -> Int,Expr->Int)


--Stmt
generateStmtCode::  CodeGenLookups -> Stmt -> [ByteCodeMnemonic]

generateStmtCode lookup (TypedStmt(Block([]),typeName))  = []
generateStmtCode lookup (TypedStmt(Block((stmt:stmts)),typeName)) = (generateStmtCode lookup stmt) ++ (generateStmtCode lookup (TypedStmt((Block(stmts)),typeName)))


generateStmtCode lookup (TypedStmt(Return(typedExp),typeName)) = let expType = getTypeFromExpr typedExp
                                    in
                                    if expType == "void" then [RETURN]
                                    else if expType == "int" ||  expType == "char" || expType == "boolean" then (generateExprCode lookup typedExp) ++[IRETURN]
                                    else (generateExprCode lookup typedExp) ++ [ARETURN]


generateStmtCode lookup (TypedStmt(ReturnEmpty,_)) = [RETURN]

generateStmtCode lookup (TypedStmt(While(expr,stmt),typeName)) = let exprCode = (generateExprCode lookup expr)
                                                                     stmtCode = (generateStmtCode lookup stmt)
                                                                     exprLength = (codeLength exprCode)
                                                                     stmtLength = (codeLength stmtCode)
                                                                     jumpOffset = stmtLength+3+3 --Take into account the GOTO command and the IF_ICMPNE command itselfe 
                                                                     jumpBackOffset = (exprLength+1+3+stmtLength)*(-1) -- expr + ifconst_1 + if_icmpne + stmt
                                                                   in
                                                                   exprCode ++ [ICONST_1,(IF_ICMPNE(jumpOffset))] ++ stmtCode ++ [(GOTO(jumpBackOffset))] 


generateStmtCode lookup (TypedStmt(If(expr, stmt1 , Just stmt2),typeName)) = let exprCode = (generateExprCode lookup expr)
                                                                                 stmt1Code = (generateStmtCode lookup stmt1)
                                                                                 stmt1Length = (codeLength stmt1Code) + 3 --Take into account the IF_ICMPNE command itselfe
                                                                                 stmt2Code = (generateStmtCode lookup stmt2)
                                                                                 stmt2Length = codeLength stmt2Code
                                                                           in
                                                                           exprCode ++ [ICONST_1,IF_ICMPNE(stmt1Length)] ++ stmt1Code ++ stmt2Code

generateStmtCode lookup (TypedStmt(If(expr, stmt1 , Nothing),typeName)) = let exprCode = (generateExprCode lookup expr)
                                                                              stmtCode = (generateStmtCode lookup stmt1)
                                                                              stmtLength = (codeLength stmtCode) + 3 --Take into account the IF_ICMPNE command itselfe
                                                                            in
                                                                            exprCode ++ [ICONST_1,(IF_ICMPNE(stmtLength))] ++ stmtCode

generateStmtCode lookup (TypedStmt(StmtExprStmt(stmtExpr),typeName))  = generateStmtExprCode lookup stmtExpr

--Var decls do not have any represenation in the byte code. The are analysed/indexed in the method gen module.
generateStmtCode lookup (TypedStmt((LocalVarDecl(_,_)),_)) = []

--StmtExpr
generateStmtExprCode:: CodeGenLookups -> StmtExpr -> [ByteCodeMnemonic]
generateStmtExprCode (CodeGenLookups(varLookup,b,c,d,e)) (TypedStmtExpr(Assign(TypedExpr(LocalVar(varName),typeName), expr),"void")) =  let exprCode = (generateExprCode (CodeGenLookups(varLookup,b,c,d,e)) expr)
                                                                                                                                            varIndex = (varLookup varName)
                                                                                                                              in
                                                                                                                              if typeName == "int" || typeName== "char" || typeName=="boolean" then exprCode++ [ISTORE(varIndex)]
                                                                                                                              else exprCode++ [ASTORE(varIndex)]

generateStmtExprCode (CodeGenLookups(varLookup,b,c,d,e)) (TypedStmtExpr(Assign(TypedExpr(LocalVar(varName),typeName), expr),assignType)) =  let exprCode = (generateExprCode (CodeGenLookups(varLookup,b,c,d,e)) expr)
                                                                                                                                                varIndex = (varLookup varName) in
                                                                                                                              if typeName == "int" || typeName== "char" || typeName=="boolean" then exprCode++ [DUP,ISTORE(varIndex)]
                                                                                                                              else exprCode++ [DUP,ASTORE(varIndex)]



generateStmtExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) (TypedStmtExpr(Assign(TypedExpr(InstVar(destExpr, fieldName),typeName), sourceExpr),"void")) = let destExprCode = (generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) destExpr)
                                                                                                                                                              sourceExprCode = (generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) sourceExpr)
                                                                                                                                                              fieldIndex = (fieldLookup (getTypeFromExpr destExpr) fieldName)
                                                                                                                                                in
                                                                                                                                                destExprCode ++  sourceExprCode ++ [(PUTFIELD(fieldIndex))]

generateStmtExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) (TypedStmtExpr(Assign(TypedExpr(InstVar(destExpr, fieldName),_), sourceExpr),_)) = let destExprCode = (generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) destExpr)
                                                                                                                                                  sourceExprCode = (generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) sourceExpr)
                                                                                                                                                  fieldIndex = (fieldLookup (getTypeFromExpr destExpr) fieldName)
                                                                                                                                      in
                                                                                                                                        destExprCode ++  sourceExprCode  ++ [DUP_X1,(PUTFIELD(fieldIndex))] --Duplicates the top item on the stack and inserts the duplicate below the second-from-top item



generateStmtExprCode lookup (TypedStmtExpr(New(typeName, (expr:exprs)),returnType))   =  (generateExprCode lookup expr) ++ (generateStmtExprCode lookup (TypedStmtExpr(New(typeName, exprs),returnType)))

generateStmtExprCode (CodeGenLookups(a,methodLookup,c,typeLookup,e)) (TypedStmtExpr(New(typeName, []),_))  = [(NEW((typeLookup typeName))),DUP,(INVOKESPECIAL(methodLookup typeName "<init>"))]

generateStmtExprCode lookup (TypedStmtExpr(MethodCall(sourceExpr, methodName, (expr:exprs)),typeName)) = (generateExprCode lookup sourceExpr) ++ (generateExprCode lookup expr) ++ (generateStmtExprCode lookup (TypedStmtExpr(MethodCall(sourceExpr, methodName, exprs),typeName)))
generateStmtExprCode (CodeGenLookups(a,methodLookup,c,d,e)) (TypedStmtExpr(MethodCall(sourceExpr, methodName, []),typeName))  = let lookup = (CodeGenLookups(a,methodLookup,c,d,e))
                                                                                                                        in
                                                                                                                        (generateExprCode lookup sourceExpr) ++[(INVOKESPECIAL(methodLookup (getTypeFromExpr sourceExpr) methodName))]


--Expr
generateExprCode:: CodeGenLookups -> Expr  -> [ByteCodeMnemonic]


generateExprCode (CodeGenLookups(varLookup,b,c,d,e)) (TypedExpr((LocalVar(varName)),typeName))  = let varIndex = (varLookup varName)
                                                                                  in
                                                                                  if  typeName == "int" || typeName== "char" || typeName=="boolean" then [(ILOAD(varLookup varName))]
                                                                                  else [(ALOAD(varLookup varName))]

generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) (TypedExpr((InstVar(expr,fieldName)),typeName))  = let fieldIndex = (fieldLookup (getTypeFromExpr expr) fieldName)
                                                                                           in
                                                                                           (generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) expr) ++ [(GETFIELD(fieldIndex))]



generateExprCode lookup (TypedExpr(Unary("!", exp),typeName))  =  (generateExprCode lookup exp)++ [IFEQ(7),ICONST_0,GOTO(4),ICONST_1]
generateExprCode lookup (TypedExpr(Unary("+", exp),typeName)) = (generateExprCode lookup exp) ++ []
generateExprCode lookup (TypedExpr(Unary("-", exp),typeName)) =  (generateExprCode lookup exp) ++[INEG]


generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) (TypedExpr(Unary(operator, (TypedExpr(InstVar(instanceExpr,fieldName),typeName))),"void")) = let fieldIndex = (fieldLookup (getTypeFromExpr instanceExpr) fieldName)
                                                                                                                                                        lookup = (CodeGenLookups(a,b,fieldLookup,d,e))
                                                                                                                                                        destExprCode = (generateExprCode lookup instanceExpr)
                                                                                                                                                        sourceExprCode = (generateExprCode lookup (TypedExpr(InstVar(instanceExpr,fieldName),typeName)))
                                                                                                                                           in
                                                                                                                                           if operator == "++" then destExprCode ++ sourceExprCode ++[ICONST_1,IADD] ++ [(PUTFIELD(fieldIndex))]
                                                                                                                                           else destExprCode ++ sourceExprCode ++[ICONST_1,ISUB] ++ [(PUTFIELD(fieldIndex))]

generateExprCode (CodeGenLookups(a,b,fieldLookup,d,e)) (TypedExpr(Unary(operator, (TypedExpr(InstVar(instanceExpr,fieldName),typeName))),"int")) = let fieldIndex = (fieldLookup (getTypeFromExpr instanceExpr) fieldName)
                                                                                                                                                       lookup = (CodeGenLookups(a,b,fieldLookup,d,e))
                                                                                                                                                       destExprCode = (generateExprCode lookup instanceExpr)
                                                                                                                                                       sourceExprCode = (generateExprCode lookup (TypedExpr(InstVar(instanceExpr,fieldName),typeName)))
                                                                                                                                          in
                                                                                                                                          if operator == "++" then destExprCode ++ sourceExprCode ++[ICONST_1,IADD,DUP_X1,(PUTFIELD(fieldIndex))]
                                                                                                                                          else destExprCode ++ sourceExprCode ++[ICONST_1,ISUB,DUP_X1,(PUTFIELD(fieldIndex))]



generateExprCode lookup (TypedExpr(Binary("+", exp1, exp2),typeName)) =(generateExprCode lookup exp1)++ (generateExprCode lookup exp2)++[IADD]
generateExprCode lookup (TypedExpr(Binary("-", exp1, exp2),typeName)) =(generateExprCode lookup exp1)++ (generateExprCode lookup exp2)++[ISUB]
generateExprCode lookup (TypedExpr(Binary("*", exp1, exp2),typeName)) =(generateExprCode lookup exp1)++ (generateExprCode lookup exp2)++[IMUL]
generateExprCode lookup (TypedExpr(Binary("/", exp1, exp2),typeName)) =(generateExprCode lookup exp1)++ (generateExprCode lookup exp2)++[IDIV]
generateExprCode lookup (TypedExpr(Binary("%", exp1, exp2),typeName)) =(generateExprCode lookup exp1)++ (generateExprCode lookup exp2)++[IREM]
generateExprCode lookup (TypedExpr(Binary("==", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPEQ(7),ICONST_1,GOTO(4),ICONST_0]
generateExprCode lookup (TypedExpr(Binary("!=", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPNE(7),ICONST_0,GOTO(4),ICONST_1]
generateExprCode lookup (TypedExpr(Binary(">", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPGT(7),ICONST_0,GOTO(4),ICONST_1]
generateExprCode lookup (TypedExpr(Binary("<", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPLT(7),ICONST_0,GOTO(4),ICONST_1] 
generateExprCode lookup (TypedExpr(Binary(">=", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPGE(7),ICONST_0,GOTO(4),ICONST_1]
generateExprCode lookup (TypedExpr(Binary("<=", exp1, exp2),typeName)) = (generateExprCode lookup exp1) ++ (generateExprCode lookup exp2) ++ [IF_ICMPLE(7),ICONST_0,GOTO(4),ICONST_1] 


generateExprCode lookup (This) = [ALOAD_0]
generateExprCode lookup (TypedExpr(This,typeName)) = [ALOAD_0]
generateExprCode lookup (TypedExpr(Jnull,typeName)) = [ACONST_NULL]
generateExprCode (CodeGenLookups(a,b,c,d,constExpr)) (TypedExpr(String(x),typeName)) = [(LDC(constExpr(String(x))))]
generateExprCode (CodeGenLookups(a,b,c,d,constExpr)) (TypedExpr(Integer(x),typeName)) = [(LDC(constExpr(Integer(x))))]
generateExprCode lookup (TypedExpr(Bool(True),typeName)) = [ICONST_1]
generateExprCode lookup (TypedExpr(Bool(False),typeName)) = [ICONST_0]
generateExprCode lookup (TypedExpr(Char(x),typeName)) = [(BIPUSH(ord(x)))]
generateExprCode lookup (TypedExpr(StmtExprExpr(stmtExpr),typeName)) = generateStmtExprCode lookup stmtExpr