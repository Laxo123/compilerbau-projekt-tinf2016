module ConstPool(fillConstPoolStart) where
import Abstract_syntax.AbsSyn
import Libraries.Data.ClassFormat
import Descriptor

type ConstPool = [(CP_Info, Int)]

instance Eq Tag where
   TagClass == TagClass = True
   TagFieldRef == TagFieldRef = True
   TagMethodRef == TagMethodRef = True
   TagInterfaceMethodRef == TagInterfaceMethodRef = True
   TagString == TagString = True
   TagInteger == TagInteger = True
   TagFloat == TagFloat = True
   TagLong == TagLong = True
   TagDouble == TagDouble = True
   TagNameAndType == TagNameAndType = True
   TagUtf8 == TagUtf8 = True
   _ == _ = False

instance Eq CP_Info where
   (Class_Info a b c) == (Class_Info aa bb cc) = (a == aa) && (b == bb) && (c == cc)
   (FieldRef_Info a b c d) == (FieldRef_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (MethodRef_Info a b c d) == (MethodRef_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (InterfaceMethodRef_Info a b c d) == (InterfaceMethodRef_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (String_Info a b c) == (String_Info aa bb cc) = (a == aa) && (b == bb) && (c == cc)
   (Integer_Info a b c) == (Integer_Info aa bb cc) = (a == aa) && (b == bb) && (c == cc)
   (Float_Info a b c) == (Float_Info aa bb cc) = (a == aa) && (b == bb) && (c == cc)
   (Long_Info a b c d) == (Long_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (Double_Info a b c d) == (Double_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (NameAndType_Info a b c d) == (NameAndType_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   (Utf8_Info a b c d) == (Utf8_Info aa bb cc dd) = (a == aa) && (b == bb) && (c == cc) && (d == dd)
   _ == _ = False


--Zum starten des erstellens des Konstantenpools
fillConstPoolStart:: Prg -> [CP_Info]
fillConstPoolStart prg = map fst (insert_MethodCall (fillConstantPoolPrg [((Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""}), 1)] prg) "java/lang/Object" (TypedStmtExpr((MethodCall(TypedExpr(This, "java/lang/Object"), "<init>", [])), "void")))
   
--Das Auslesen der Klassen im Programm
fillConstantPoolPrg::ConstPool -> Prg -> ConstPool
fillConstantPoolPrg const [] = const
fillConstantPoolPrg const [oneClass] = insert_ClassInfo (fillConstantPoolClass const oneClass) (Class("java/lang/Object", [], []))
fillConstantPoolPrg const (moreClass:rest) = fillConstantPoolPrg (fillConstantPoolClass const moreClass) rest

--Für Klassen
fillConstantPoolClass:: ConstPool -> Class -> ConstPool
fillConstantPoolClass const (Class(typeClass, fieldClass, methodClass)) = fillConstantPoolMethod (fillConstantPoolField (insert_ClassInfo const (Class(typeClass, fieldClass, methodClass))) fieldClass) methodClass typeClass


--Für die Felder
fillConstantPoolField:: ConstPool -> [FieldDecl] -> ConstPool
fillConstantPoolField const [] = const
fillConstantPoolField const ((FieldDecl(typeField, stringField)):rest) = let returnTypeDes = getFieldDescriptor typeField
                                                                             (constPoolReturn, _) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length returnTypeDes), cad_cp=returnTypeDes, desc=""}))
                                                                             (constPoolReturn1, _) = (insert constPoolReturn (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length stringField), cad_cp=stringField, desc=""})) in
                                                                             fillConstantPoolField constPoolReturn1 rest


--Für die Methoden
fillConstantPoolMethod:: ConstPool -> [MethodDecl] -> String -> ConstPool
fillConstantPoolMethod const [] currentClass = const
fillConstantPoolMethod const [Method(typeMethod, stringMethod, attributeMethod, statementMethod)] currentClass = fillConstantPoolStmt (insert_Method const (Method(typeMethod, stringMethod, attributeMethod, statementMethod))) [statementMethod] currentClass
fillConstantPoolMethod const (Method(typeMethod, stringMethod, attributeMethod, statementMethod):methods) currentClass = fillConstantPoolMethod (fillConstantPoolStmt (insert_Method const (Method(typeMethod, stringMethod, attributeMethod, statementMethod))) [statementMethod] currentClass) methods currentClass


--Für die Statements
fillConstantPoolStmt:: ConstPool -> [Stmt] -> String -> ConstPool
fillConstantPoolStmt const [TypedStmt(stmtTyped, typeTyped)] currentClass = fillConstantPoolStmt const [stmtTyped] currentClass
fillConstantPoolStmt const [Block(stmtBlock)] currentClass = fillConstantPoolStmt const stmtBlock currentClass
fillConstantPoolStmt const [Return(exprReturn)] currentClass = fillConstantPoolExpr const exprReturn currentClass
fillConstantPoolStmt const [While(exprWhile, stmtWhile)] currentClass = fillConstantPoolStmt (fillConstantPoolExpr const exprWhile currentClass) [stmtWhile] currentClass
fillConstantPoolStmt const [If(exprIf, stmtIf, Nothing)] currentClass = fillConstantPoolStmt (fillConstantPoolExpr const exprIf currentClass) [stmtIf] currentClass
fillConstantPoolStmt const [If(exprIf, stmtIf, Just stmtElse)] currentClass = fillConstantPoolStmt (fillConstantPoolStmt (fillConstantPoolExpr const exprIf currentClass) [stmtIf] currentClass) [stmtElse] currentClass
fillConstantPoolStmt const [StmtExprStmt(stmtExprStmtExprStmt)] currentClass = fillConstantPoolStmtExpr const stmtExprStmtExprStmt currentClass
fillConstantPoolStmt const [LocalVarDecl(_, _)] currentClass = const
fillConstantPoolStmt const (stmtOne:stmtRest) currentClass = fillConstantPoolStmt (fillConstantPoolStmt const [stmtOne] currentClass) stmtRest currentClass
fillConstantPoolStmt const _ currentClass = const


--Für die Expressions
fillConstantPoolExpr:: ConstPool -> Expr -> String -> ConstPool
fillConstantPoolExpr const (TypedExpr((InstVar(This, stringInstVar)), stringTypeV)) currentClass = insert_FieldRef_Info const currentClass (TypedExpr((InstVar(This, stringInstVar)), stringTypeV))
fillConstantPoolExpr const (TypedExpr((InstVar((TypedExpr(This, className)), stringInstVar)), stringTypeV)) currentClass = insert_FieldRef_Info const currentClass (TypedExpr((InstVar(This, stringInstVar)), stringTypeV))
fillConstantPoolExpr const (TypedExpr(exprTyped, typeTyped)) currentClass = fillConstantPoolExpr const exprTyped currentClass
fillConstantPoolExpr const (InstVar(exprInstVar, stringInstVar)) currentClass = fillConstantPoolExpr const exprInstVar currentClass
fillConstantPoolExpr const (Unary(stringUnary, exprUnary)) currentClass = fillConstantPoolExpr const exprUnary currentClass
fillConstantPoolExpr const (Binary(stringBinary, expr1Binary, expr2Binary)) currentClass = fillConstantPoolExpr (fillConstantPoolExpr const expr1Binary currentClass) expr2Binary currentClass
fillConstantPoolExpr const (Integer(integerI)) currentClass = insert_Integer const (Integer(integerI))
fillConstantPoolExpr const (String(stringI)) currentClass = insert_String const (String(stringI))
fillConstantPoolExpr const (StmtExprExpr(stmtExprI)) currentClass = fillConstantPoolStmtExpr const stmtExprI currentClass
fillConstantPoolExpr const _ currentClass = const


--Für die StatementExpressions
fillConstantPoolStmtExpr:: ConstPool -> StmtExpr -> String -> ConstPool
fillConstantPoolStmtExpr const (TypedStmtExpr((MethodCall(exprMethodCall, stringMethodCall, exprsMethodCall)), typeTyped)) currentClass = fillConstantPoolExprList (fillConstantPoolExpr (insert_MethodCall const currentClass (TypedStmtExpr((MethodCall(exprMethodCall, stringMethodCall, exprsMethodCall)), typeTyped))) exprMethodCall currentClass) exprsMethodCall currentClass
fillConstantPoolStmtExpr const (TypedStmtExpr(stmtExprTyped, typeTyped)) currentClass = fillConstantPoolStmtExpr const stmtExprTyped currentClass
fillConstantPoolStmtExpr const (Assign(expr1Assign, expr2Assign)) currentClass = fillConstantPoolExpr (fillConstantPoolExpr const expr1Assign currentClass) expr2Assign currentClass
fillConstantPoolStmtExpr const (New(typeNew, exprNew)) currentClass = insert_MethodCall const currentClass (TypedStmtExpr((MethodCall(TypedExpr(Jnull, typeNew), "<init>", exprNew)), "void"))
fillConstantPoolStmtExpr const _ currentClass = const


--Für eine Liste von Expressions
fillConstantPoolExprList:: ConstPool -> [Expr] -> String -> ConstPool
fillConstantPoolExprList const [] currentClass = const
fillConstantPoolExprList const [exprOne] currentClass = fillConstantPoolExpr const exprOne currentClass
fillConstantPoolExprList const (exprOne:exprRest) currentClass = fillConstantPoolExprList (fillConstantPoolExpr const exprOne currentClass) exprRest currentClass


--Schaut ob eine CP_Info bereits drin ist und fügt sie, wenn nicht, ein
--Gibt zudem den Index der gesuchten/eingefügten CP_info zurück
insert:: ConstPool -> CP_Info -> (ConstPool, Int)
insert [] cpInfo = ([(cpInfo, 1)], 1)
insert [(cpInfoLast, indexToCount)] cpInfo = if (cpInfoLast == cpInfo) 
                                             then ([(cpInfoLast, indexToCount)], indexToCount)
                                             else ([(cpInfoLast, indexToCount),(cpInfo, indexToCount+1)], indexToCount+1)
insert ((anyCPInfo, oldIndex):rest) cpInfo = if (anyCPInfo == cpInfo)
                                             then (((anyCPInfo, oldIndex):rest), oldIndex)
                                             else let (returnV, returnIndex) = insert rest cpInfo in
                                                  (((anyCPInfo, oldIndex):returnV), returnIndex)


--Fügt Klassen ein
insert_ClassInfo:: ConstPool -> Class -> ConstPool
insert_ClassInfo const (Class(typeClass, _, _)) = let (constPoolReturn, indexToUse) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length typeClass), cad_cp=typeClass, desc=""}))
                                                      (constPoolReturnFinal, _) = insert constPoolReturn (Class_Info{tag_cp=TagClass, index_cp=indexToUse, desc=""}) in
                                                      constPoolReturnFinal


--Fügt Strings ein
insert_String:: ConstPool -> Expr -> ConstPool
insert_String const (String(stringE)) = let (constPoolReturn, indexToUse) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length stringE), cad_cp=stringE, desc=""}))
                                            (constPoolReturnFinal, _) = (insert constPoolReturn (String_Info{tag_cp=TagString, index_cp=indexToUse, desc=""})) in
                                            constPoolReturnFinal


--Fügt Integer ein
insert_Integer:: ConstPool -> Expr -> ConstPool
insert_Integer const (Integer(integerE)) = let (constPoolReturnFinal, _) = (insert const (Integer_Info{tag_cp=TagInteger, numi_cp=(fromIntegral integerE), desc=""})) in
                                                                           constPoolReturnFinal


--Fügt Methodendeklarationen ein
insert_Method:: ConstPool -> MethodDecl -> ConstPool
insert_Method const (Method(methodReturnType, methodName, attr, rest)) = let (constPoolReturn, _) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length methodName), cad_cp=methodName, desc=""}))
                                                                             returnTypeWithAttr = (getMethodDescriptor (Method(methodReturnType, methodName, attr, rest)))
                                                                             (constPoolReturnFinal, _) = (insert constPoolReturn (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length returnTypeWithAttr), cad_cp=returnTypeWithAttr, desc=""})) in
                                                                             constPoolReturnFinal


--Fügt Feldreferenzen ein
insert_FieldRef_Info:: ConstPool -> String -> Expr -> ConstPool
insert_FieldRef_Info const currentClass (TypedExpr((InstVar(This, stringNameV)), stringTypeV)) = let (constPoolReturn1, indexToUse) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length currentClass), cad_cp=currentClass, desc=""}))
                                                                                                     (constPoolReturn2, indexToUseName) = (insert constPoolReturn1 (Class_Info{tag_cp=TagClass, index_cp=indexToUse, desc=""}))
                                                                                                     (constPoolReturn3, indexToUseNameV) = (insert constPoolReturn2 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length stringNameV), cad_cp=stringNameV, desc=""}))
                                                                                                     typeVUseToInsert = (getFieldDescriptor stringTypeV)
                                                                                                     (constPoolReturn4, indexToUseTypeV) = (insert constPoolReturn3 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length typeVUseToInsert), cad_cp=typeVUseToInsert, desc=""}))
                                                                                                     (constPoolReturn5, indexToUseNameAndType) = (insert constPoolReturn4 (NameAndType_Info{tag_cp=TagNameAndType, index_name_cp=indexToUseNameV, index_descr_cp=indexToUseTypeV, desc=""}))
                                                                                                     (constPoolReturnFinal, _) = insert constPoolReturn5 (FieldRef_Info{tag_cp=TagFieldRef, index_name_cp=indexToUseName, index_nameandtype_cp=indexToUseNameAndType, desc=""}) in
                                                                                                     constPoolReturnFinal


--Fügt Methodenaufrufe ein
insert_MethodCall:: ConstPool -> String -> StmtExpr -> ConstPool
insert_MethodCall const currentClass (TypedStmtExpr((MethodCall(TypedExpr(This, _), methodName, methodAttr)), typeReturn)) = let (constPoolReturn1, indexToUseName) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length methodName), cad_cp=methodName, desc=""}))
                                                                                                                                 returnTypeWithAttr = (return_Attr_MethodCall methodAttr) 
                                                                                                                                 returnToUseMethod = (getMethodDescriptor (Method(typeReturn, "", returnTypeWithAttr, Block([])))) in
                                                                                                                                 let (constPoolReturn2, indexToUseDescr) = (insert constPoolReturn1 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length returnToUseMethod), cad_cp=returnToUseMethod, desc=""}))
                                                                                                                                     (constPoolReturn3, indexToUseNameAndType) = (insert constPoolReturn2 (NameAndType_Info{tag_cp=TagNameAndType, index_name_cp=indexToUseName, index_descr_cp=indexToUseDescr, desc=""}))
                                                                                                                                     (constPoolReturn4, indexToUseClassName) = (insert constPoolReturn3 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length currentClass), cad_cp=currentClass, desc=""}))
                                                                                                                                     (constPoolReturn5, indexToUseClassInfo) = (insert constPoolReturn4 (Class_Info{tag_cp=TagClass, index_cp=indexToUseClassName, desc=""}))
                                                                                                                                     (constPoolReturnFinal, _) = (insert constPoolReturn5 (MethodRef_Info {tag_cp=TagMethodRef, index_name_cp=indexToUseClassInfo, index_nameandtype_cp=indexToUseNameAndType, desc=""})) in
                                                                                                                                     constPoolReturnFinal
insert_MethodCall const currentClass (TypedStmtExpr((MethodCall(TypedExpr(_, typeOther), methodName, methodAttr)), typeReturn)) = let (constPoolReturn1, indexToUseName) = (insert const (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length methodName), cad_cp=methodName, desc=""}))
                                                                                                                                      returnTypeWithAttr = (return_Attr_MethodCall methodAttr) 
                                                                                                                                      returnToUseMethod = (getMethodDescriptor (Method(typeReturn, "", returnTypeWithAttr, Block([])))) in
                                                                                                                                      let (constPoolReturn2, indexToUseDescr) = (insert constPoolReturn1 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length returnToUseMethod), cad_cp=returnToUseMethod, desc=""}))
                                                                                                                                          (constPoolReturn3, indexToUseNameAndType) = (insert constPoolReturn2 (NameAndType_Info{tag_cp=TagNameAndType, index_name_cp=indexToUseName, index_descr_cp=indexToUseDescr, desc=""}))
                                                                                                                                          (constPoolReturn4, indexToUseClassName) = (insert constPoolReturn3 (Utf8_Info{tag_cp=TagUtf8, tam_cp=(length typeOther), cad_cp=typeOther, desc=""}))
                                                                                                                                          (constPoolReturn5, indexToUseClassInfo) = (insert constPoolReturn4 (Class_Info{tag_cp=TagClass, index_cp=indexToUseClassName, desc=""}))
                                                                                                                                          (constPoolReturnFinal, _) = (insert constPoolReturn5 (MethodRef_Info {tag_cp=TagMethodRef, index_name_cp=indexToUseClassInfo, index_nameandtype_cp=indexToUseNameAndType, desc=""})) in
                                                                                                                                          constPoolReturnFinal


--Gibt ein Tupel zurück um die Indexe der Methodendeklaration zu erhalten, welche für den Methodenaufruf nötig sind
return_Attr_MethodCall:: [Expr] -> [(Type, String)]
return_Attr_MethodCall [(TypedExpr(stringExpr, typeExpr))] = [(typeExpr, "")]
return_Attr_MethodCall ((TypedExpr(stringExpr, typeExpr)):rest) = [(typeExpr, "")] ++ (return_Attr_MethodCall rest)
return_Attr_MethodCall _ = []

--Tests
testInsert1 = insert_Integer [] (Integer(10))

testInsert2 = insert_String (insert_Integer (insert_String [] (String("Hallo Welt!"))) (Integer(10))) (String("Hallo Welt nochmal!"))

testInsert3 = insert_Integer (insert_Integer (insert_Integer (insert_Integer (insert_Integer [] (Integer(10))) (Integer(20))) (Integer(30))) (Integer(20))) (Integer(30))

testInsert4 = insert_String (insert_Method [] (Method("Ljava/lang/String", "MethodeA",[("int","eins"), ("Ljava/lang/String","zwei"), ("boolean","drei")], Return(String("ReturnWert"))))) (String("Hallo Welt!"))

testInsert5 = insert_MethodCall (insert_Method [] (Method("Ljava/lang/String", "MethodeA",[("int","eins"), ("Ljava/lang/String","zwei"), ("boolean","drei")], Return(String("ReturnWert"))))) "currentClass" (TypedStmtExpr(MethodCall(TypedExpr(This, "this"), "MethodeA", [TypedExpr(LocalVar("eins"),"int"), TypedExpr(LocalVar("zwei"),"Ljava/lang/String"), TypedExpr(LocalVar("drei"),"boolean")]),"Ljava/lang/String"))

testA = fillConstPoolStart [Class("NeueTestKlasse", [], [])]

testB = fillConstPoolStart [Class("KlasseEins", [], []), Class("KlasseZwei", [], [])]

testC = fillConstPoolStart [Class("NeueTestKlasse", [], [Method("java/lang/String", "MethodeNummerEins",[("java/lang/String","nummerEins"), ("int","nummerZwei")], (TypedStmt(Block[TypedStmt(Return(String("ReturnDas!")), "java/lang/String")], "java/lang/String")))])]

testD = fillConstPoolStart [Class("ExternClass", [], [Method("void", "<init>", [], TypedStmt(Block [], "void"))])]

testE = fillConstPoolStart [Class("KlasseEins", [FieldDecl("int", "testFeld")], []), Class("KlasseZwei", [], [])]

testF = fillConstPoolStart [Class("NeueTestKlasse", [], [Method("java/lang/String", "MethodeNummerEins",[("java/lang/String","nummerEins")], (TypedStmt(Block[TypedStmt(StmtExprStmt(MethodCall(TypedExpr(This, "NeueTestKlasse"), "MethodeNummerEins", [String("nummerEins")])), "java/lang/String")], "java/lang/String")))])]

testG = fillConstPoolStart [Class ("Test",[],[Method ("void","<init>",[],Block []),Method ("void","call",[],Block [StmtExprStmt (TypedStmtExpr(MethodCall (TypedExpr(This, "Test"),"getcalled",[]), "void"))]),Method ("void","getcalled",[],Block [])])]

testH = fillConstPoolStart [Class("KlasseEins", [FieldDecl("java/lang/String", "text"),FieldDecl("int", "zahl"),FieldDecl("char", "zeichen"),FieldDecl("boolean", "bool")], [])]

testI = fillConstPoolStart [Class("TestClass", [], [Method("void", "myMethod",[], Block([TypedStmt(LocalVarDecl("java/lang/String", "a"), "java/lang/String"), TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("a"), "java/lang/String"), TypedExpr(String("abcd"), "java/lang/String")), "java/lang/String")), "java/lang/String"), 
        TypedStmt(LocalVarDecl("int", "b"), "int"), TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("b"), "int"), TypedExpr(Integer(42), "int")), "int")), "int"),
        TypedStmt(LocalVarDecl("char", "c"), "char"), TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("c"), "char"), TypedExpr(Char('c'), "char")), "char")), "char"),
        TypedStmt(LocalVarDecl("boolean", "d"), "boolean"), TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("d"), "boolean"), TypedExpr(Bool(True), "boolean")), "boolean")), "boolean")
        ]))])]

testJ = fillConstPoolStart [Class("KlasseEins", [FieldDecl("java/lang/String", "text"),FieldDecl("int", "zahl")], [Method("java/lang/String", "MethodeNummerEins",[], (TypedStmt(Block[TypedStmt(Return(TypedExpr(InstVar(This,"text"),"java/lang/String")), "java/lang/String")], "java/lang/String")))])]


--Beispiele
beispiel1 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void"))])]

beispiel2 = fillConstPoolStart [Class ("TestClass",[FieldDecl ("java/lang/String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],TypedStmt (Block [],"void"))])]

beispiel3 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("void","myMethod",[],TypedStmt (Block [],"void"))])]

beispiel4 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("void","myMethod",[("java/lang/String","text"),("int","zahl")],TypedStmt (Block [],"void"))])]

beispiel5 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("int","myMethod",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (Integer 42,"int")),"int")],"int"))])]

beispiel6 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("int","myMethod",[("java/lang/String","text"),("int","zahl")],TypedStmt (Block [TypedStmt (Return (TypedExpr (LocalVar "zahl","int")),"int")],"int"))])]

beispiel7 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("java/lang/String","myMethod",[("java/lang/String","text"),("int","zahl")],TypedStmt (Block [TypedStmt (Return (TypedExpr (LocalVar "text","java/lang/String")),"java/lang/String")],"java/lang/String"))])]

beispiel8 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("int","myMethod",[("int","a"),("int","b")],TypedStmt (Block [TypedStmt (Return (TypedExpr (Binary ("+",TypedExpr (Binary ("+",TypedExpr (LocalVar "a","int"),TypedExpr (Binary ("*",TypedExpr (LocalVar "b","int"),TypedExpr (LocalVar "b","int")),"int")),"int"),TypedExpr (Binary ("%",TypedExpr (Binary ("/",TypedExpr (LocalVar "a","int"),TypedExpr (LocalVar "a","int")),"int"),TypedExpr (LocalVar "b","int")),"int")),"int")),"int")],"int"))])]

beispiel9 = fillConstPoolStart []

beispiel10 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("java/lang/Object","myMethod",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (String "Hello World","java/lang/String")),"java/lang/String")],"java/lang/String"))])]

beispiel11 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("void","myMethod",[],TypedStmt (Block [TypedStmt (LocalVarDecl ("java/lang/String","a"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "a","java/lang/String"),TypedExpr (String "abcd","java/lang/String")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("int","b"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "b","int"),TypedExpr (Integer 42,"int")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("char","c"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "c","char"),TypedExpr (Char 'c',"char")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("boolean","d"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "d","boolean"),TypedExpr (Bool True,"boolean")),"boolean")),"boolean")],"java/lang/Object"))])]

beispiel12 = fillConstPoolStart [Class ("TestClass",[FieldDecl ("java/lang/String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("java/lang/String","getText",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"text"),"java/lang/String")),"java/lang/String")],"java/lang/String")),Method ("int","getZahl",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"zahl"),"int")),"int")],"int")),Method ("char","getZeichen",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"zeichen"),"char")),"char")],"char")),Method ("boolean","getBool",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"bool"),"boolean")),"boolean")],"boolean"))])]

beispiel13 = fillConstPoolStart [Class ("TestClass",[FieldDecl ("java/lang/String","text"),FieldDecl ("int","zahl"),FieldDecl ("char","zeichen"),FieldDecl ("boolean","bool")],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("void","setText",[("java/lang/String","value")],TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"text"),"java/lang/String"),TypedExpr (LocalVar "value","java/lang/String")),"boolean")),"boolean")],"boolean")),Method ("void","setZahl",[("int","value")],TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"zahl"),"int"),TypedExpr (LocalVar "value","int")),"boolean")),"boolean")],"boolean")),Method ("void","setZeichen",[("char","value")],TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"zeichen"),"char"),TypedExpr (LocalVar "value","char")),"boolean")),"boolean")],"boolean")),Method ("void","setBool",[("boolean","value")],TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (InstVar ((TypedExpr(This, "TestClass")),"bool"),"boolean"),TypedExpr (LocalVar "value","boolean")),"boolean")),"boolean")],"boolean"))])]

beispiel14 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("boolean","myMethod",[("boolean","value")],TypedStmt (Block [TypedStmt (If (TypedExpr (LocalVar "value","boolean"),TypedStmt (Block [TypedStmt (Return (TypedExpr (Bool False,"boolean")),"boolean")],"boolean"),Nothing),"boolean"),TypedStmt (Return (TypedExpr (Bool True,"boolean")),"boolean")],"boolean"))])]

beispiel15 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("boolean","myMethod",[("int","a"),("int","b")],TypedStmt (Block [TypedStmt (If (TypedExpr (Binary (">",TypedExpr (LocalVar "a","int"),TypedExpr (LocalVar "b","int")),"boolean"),TypedStmt (Block [TypedStmt (Return (TypedExpr (Bool True,"boolean")),"boolean")],"boolean"),Just (TypedStmt (Block [TypedStmt (Return (TypedExpr (Bool False,"boolean")),"boolean")],"boolean"))),"boolean")],"boolean"))])]

beispiel16 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("int","myMethod",[],TypedStmt (Block [TypedStmt (LocalVarDecl ("int","counter"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 0,"int")),"boolean")),"boolean"),TypedStmt (While (TypedExpr (Binary ("<",TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 10,"int")),"boolean"),TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "counter","int"),TypedExpr (Binary ("+",TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 1,"int")),"int")),"boolean")),"boolean")],"boolean")),"boolean"),TypedStmt (Return (TypedExpr (LocalVar "counter","int")),"int")],"java/lang/Object"))])]

beispiel17 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("int","methodA",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (StmtExprExpr (TypedStmtExpr (MethodCall (TypedExpr (This,"TestClass"),"methodB",[]),"int")),"int")),"int")],"int")),Method ("int","methodB",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (Integer 42,"int")),"int")],"int"))])]

beispiel18 = fillConstPoolStart [Class ("TestClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void")),Method ("BClass","myMethod",[],TypedStmt (Block [TypedStmt (Return (TypedExpr (StmtExprExpr (TypedStmtExpr (New ("BClass",[]),"BClass")),"BClass")),"BClass")],"BClass"))]),Class ("BClass",[],[Method ("void","<init>",[],TypedStmt (Block [],"void"))])]

beispiel19 = fillConstPoolStart []

beispiel20 = fillConstPoolStart []















