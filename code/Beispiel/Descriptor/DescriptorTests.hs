import Descriptor
import Abstract_syntax.AbsSyn

emptyMethodDescriptor = getMethodDescriptor (Method("void", "myMethod",[], Block([])))
baseTypeParameterDescriptor = getMethodDescriptor (Method("void", "myMethod",[("int","a"),("boolean","b")], Block([])))
objectTypeParameterDescriptor = getMethodDescriptor (Method("void", "myMethod",[("int","a"),("java/lang/Object","b")], Block([])))