import Abstract_syntax.AbsSyn
import CodeGen
import qualified Data.Map as Map
import Data.Maybe
import MethodGen

abssyn_block_of_vardecls = TypedStmt(Block([
                                  (TypedStmt(LocalVarDecl("int", "a"),"void")),
                                  (TypedStmt(LocalVarDecl("String", "b"),"void")),
                                  (TypedStmt(LocalVarDecl("Char", "c"),"void")),
                                  (TypedStmt(LocalVarDecl("int", "d"),"void")),
                                  (TypedStmt(LocalVarDecl("int", "e"),"void")),
                                  (TypedStmt(LocalVarDecl("int", "f"),"void"))])
                                   ,"void")

test_getLocalVars_BlockOfVarDecls =  getLocalVarDecls abssyn_block_of_vardecls
test_numberVars_BlockOfVarDecls = numberVars test_getLocalVars_BlockOfVarDecls


abssyn_complex_BlockOfVarDecls = TypedStmt(Block([
                                                  abssyn_block_of_vardecls,
                                                  (TypedStmt(LocalVarDecl("String", "g"),"void")),
                                                  (TypedStmt(LocalVarDecl("Char", "h"),"void")),
                                                  (TypedStmt(LocalVarDecl("int", "i"),"void")),
                                                  (TypedStmt(LocalVarDecl("int", "j"),"void")),
                                                  (TypedStmt(LocalVarDecl("int", "k"),"void"))])
                                                   ,"void")

test_getLocalVars_complex_BlockOfVarDecls =  getLocalVarDecls abssyn_complex_BlockOfVarDecls
test_numberVars_complex_BlockOfVarDecls = numberVars test_getLocalVars_complex_BlockOfVarDecls




abssyn_block_of_duplicatevardecls = TypedStmt(Block([
                                                    (TypedStmt(LocalVarDecl("int", "a"),"void")),
                                                    (TypedStmt(LocalVarDecl("int", "d"),"void")),
                                                    (TypedStmt(LocalVarDecl("int", "a"),"void")),
                                                    (TypedStmt(LocalVarDecl("int", "d"),"void")),
                                                    (TypedStmt(LocalVarDecl("int", "e"),"void")),
                                                    (TypedStmt(LocalVarDecl("int", "e"),"void"))])
                                                     ,"void")

test_getLocalVars_duplicatevardecls =  getLocalVarDecls abssyn_block_of_duplicatevardecls
test_numberVars_duplicatevardecls = numberVars test_getLocalVars_duplicatevardecls