import Abstract_syntax.AbsSyn
import Libraries.Data.ClassFormat
import CodeGen
import MethodGen
import Data.Tuple
import qualified Data.Map as Map
import ConstPoolLookup

emptyClassCp = [
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "EmptyClass", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
            ]


dummyConstPoolLookup = ConstPoolLookup((\x->(1)),\typeName methodeName->(0),\typeName->(-1),(\typeName fieldName ->(1)),\expr->(-1))
fieldLookup = (\fieldOwnerType fieldName ->(-1))

data_EmpyMethodVoid= Method("void", "testEmpyMethodVoid",[], TypedStmt(Block([]),"void"))
data_EmpyConstructor= Method("void", "MyClass",[], TypedStmt(Block([]),"void"))

testEmpyMethodVoid=generateSingleMethod dummyConstPoolLookup  "MyClass" data_EmpyMethodVoid
testEmpyConstructor=generateSingleMethod dummyConstPoolLookup "MyClass" data_EmpyConstructor


