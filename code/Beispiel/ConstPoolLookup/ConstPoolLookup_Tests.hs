import Abstract_syntax.AbsSyn
import Libraries.Data.ClassFormat
import CodeGen
import MethodGen
import Data.Tuple
import qualified Data.Map as Map
import ConstPoolLookup


emptyClassCp = [
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "EmptyClass", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
            ]


ggtCp = [
             MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 12, desc = ""},
             Integer_Info {tag_cp = TagInteger, numi_cp = 450000, desc = ""},
             String_Info {tag_cp = TagString, index_cp = 13,desc = ""},
             MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 14, desc = ""},
             Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},
             Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "methodFromGGT", desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 23, cad_cp = "(IILjava/lang/String;)I", desc = ""},
             NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Test", desc = ""},
             NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "GGT", desc = ""},
             Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
             ]

classWithField = [
                            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 14, desc = ""},
                            FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 15, desc = ""},
                            String_Info {tag_cp = TagString, index_cp = 16, desc = ""},
                            FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 17, desc = ""},
                            Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},
                            Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "myField", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "myString", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 18, cad_cp = "Ljava/lang/String;", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 11, index_descr_cp = 12, desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "Hello World", desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 9, index_descr_cp = 10, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "FieldAndConst", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
                  ]

example18Constpool = [
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "TestClass", desc = ""},
                        Class_Info {tag_cp = TagClass, index_cp = 2, desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "myMethod", desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "()LBClass;", desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "BClass", desc = ""},
                        Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
                        Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},
                        Class_Info {tag_cp = TagClass, index_cp = 10, desc = ""},
                        NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},
                        MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 11, index_nameandtype_cp = 12, desc = ""},
                        Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
                        MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 14, index_nameandtype_cp = 12, desc = ""}
                     ]  

example12=[
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 9, cad_cp = "TestClass", desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 2, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 18, cad_cp = "Ljava/lang/String;", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "text", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "zahl", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "C", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "zeichen", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "Z", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "bool", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "getText", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "()LString;", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "getZahl", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()I", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "getZeichen", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()C", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "getBool", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()Z", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 22, desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 12, index_descr_cp = 13, desc = ""},
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 23, index_nameandtype_cp = 24, desc = ""}
           ]

testEmptyClassConstPoolLookup = createConstPoolLookup emptyClassCp
testEmptyClassUtf8Lookup = createUtf8InfoLookup emptyClassCp
testEmptyClassClassRefLookup= createClassRefLookup emptyClassCp
testEmptyClassMethodRefLookup = createMethodRefLookup emptyClassCp

testGgtConstPoolLookup = createConstPoolLookup ggtCp
testGgtUtf8Lookup = createUtf8InfoLookup ggtCp
testGgtClassRefLookup= createClassRefLookup ggtCp
testGgtMethodRefLookup = createMethodRefLookup ggtCp
testGgtConstLookup = createValueConstantLookup ggtCp


classWithFieldConstPoolLookup = createConstPoolLookup classWithField
classWithFieldUtf8Lookup = createUtf8InfoLookup classWithField
classWithFieldClassRefLookup= createClassRefLookup classWithField
classWithFieldMethodRefLookup = createMethodRefLookup classWithField
classWithFieldConstLookup = createValueConstantLookup classWithField
classWithFieldFieldLookup = createFieldRefLookup classWithField


example13MethodLookup =createMethodRefLookup example18Constpool

example12FieldLookup = createFieldRefLookup example12