import FieldGen
import Abstract_syntax.AbsSyn
import Libraries.Data.ClassFormat
import ConstPoolLookup

classWithField = [
                            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 14, desc = ""},
                            FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 15, desc = ""},
                            String_Info {tag_cp = TagString, index_cp = 16, desc = ""},
                            FieldRef_Info {tag_cp = TagFieldRef, index_name_cp = 5, index_nameandtype_cp = 17, desc = ""},
                            Class_Info {tag_cp = TagClass, index_cp = 18, desc = ""},
                            Class_Info {tag_cp = TagClass, index_cp = 19, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 7, cad_cp = "myField", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 1, cad_cp = "I", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 8, cad_cp = "myString", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 18, cad_cp = "Ljava/lang/String;", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 11, index_descr_cp = 12, desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 11, cad_cp = "Hello World", desc = ""},
                            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 9, index_descr_cp = 10, desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "FieldAndConst", desc = ""},
                            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
                  ]

constPoolLookup = createConstPoolLookup classWithField

fieldDecls = [(FieldDecl("java/lang/String", "myString"))]


fieldsTest = generateFields constPoolLookup fieldDecls