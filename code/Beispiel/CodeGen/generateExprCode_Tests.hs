import Abstract_syntax.AbsSyn
import CodeGen

dummyMap= (\a->(-1))
dummyLookup= (CodeGenLookups(dummyMap,\a b -> (-1),\a b -> (-1),dummyMap,dummyMap))

--Data for the following tests
data_SomeFieldName = (TypedExpr((InstVar((TypedExpr(This,"Object")),"someFieldName")),"int"))
data_SomeFieldInSomeOtherFieldName = (TypedExpr((InstVar(data_SomeFieldName,"someOtherFieldName")),"int"))
data_SomeLocalVar= (TypedExpr((LocalVar("someLocalVar")),"Object"))
data_SomeFieldInVar = (TypedExpr((InstVar(data_SomeLocalVar,"someOtherFieldName")),"int"))

--InstVar
test_InstVar_This = (generateExprCode dummyLookup data_SomeFieldName)
test_InstVarOfInstvar = (generateExprCode dummyLookup data_SomeFieldInSomeOtherFieldName)

--LocalVar
test_SomeLocalVar = (generateExprCode dummyLookup data_SomeLocalVar)
test_SomeFieldInVar = (generateExprCode dummyLookup data_SomeFieldInVar)

test_unary_not = (generateExprCode dummyLookup (TypedExpr(Unary("!", TypedExpr(Bool(True),"boolean")),"boolean")))
test_unary_positiv = (generateExprCode dummyLookup (TypedExpr(Unary("+", TypedExpr(Integer(42),"int")),"int")))
test_unary_negative = (generateExprCode dummyLookup (TypedExpr(Unary("-", TypedExpr(Integer(42),"int")),"int")))
test_unary_increment_this_field_void = (generateExprCode dummyLookup (TypedExpr(Unary("++", (TypedExpr((InstVar(TypedExpr(This,"Object"),"someFieldName")),"int"))),"void")))
test_unary_increment_this_field_int = (generateExprCode dummyLookup (TypedExpr(Unary("++", (TypedExpr((InstVar(TypedExpr(This,"Object"),"someFieldName")),"int"))),"int")))
test_unary_decrement_this_field_void = (generateExprCode dummyLookup (TypedExpr(Unary("--", (TypedExpr((InstVar(TypedExpr(This,"Object"),"someFieldName")),"int"))),"void")))
test_unary_decrement_this_field_int = (generateExprCode dummyLookup (TypedExpr(Unary("--", (TypedExpr((InstVar(TypedExpr(This,"Object"),"someFieldName")),"int"))),"int")))

test_Binary_Plus = (generateExprCode dummyLookup (TypedExpr(Binary("+", TypedExpr(Integer(42),"int"), TypedExpr(Integer(42),"int")),"int")))
test_Binary_Minus = (generateExprCode dummyLookup (TypedExpr(Binary("-", TypedExpr(Integer(42),"int"), TypedExpr(Integer(42),"int")),"int")))
test_Binary_Mul = (generateExprCode dummyLookup (TypedExpr(Binary("*", TypedExpr(Integer(42),"int"), TypedExpr(Integer(42),"int")),"int")))
test_Binary_Div = (generateExprCode dummyLookup (TypedExpr(Binary("/", TypedExpr(Integer(42),"int"), TypedExpr(Integer(42),"int")),"int")))
test_Binary_Mod = (generateExprCode dummyLookup (TypedExpr(Binary("%", TypedExpr(Integer(42),"int"), TypedExpr(Integer(42),"int")),"int")))

test_This= (generateExprCode dummyLookup (TypedExpr(This,"Type")))
test_Jnull= (generateExprCode dummyLookup (TypedExpr(Jnull,"Type")))
test_String= (generateExprCode dummyLookup (TypedExpr(String("Stringkonstante"),"String")))
test_Integer= (generateExprCode dummyLookup (TypedExpr(Integer(42),"int")))
test_True= (generateExprCode dummyLookup (TypedExpr(Bool(True),"boolean")))
test_False= (generateExprCode dummyLookup (TypedExpr(Bool(False),"boolean")))
test_char= (generateExprCode dummyLookup (TypedExpr(Char('A'),"char")))

test_instVar = generateExprCode dummyLookup (TypedExpr (InstVar (This,"text"),"java/lang/String"))