import Abstract_syntax.AbsSyn
import CodeGen

dummyMap= (\a->(-1))
dummyLookup= (CodeGenLookups(dummyMap,\a b -> (-1),\a b -> (-1),dummyMap,dummyMap))

--Data for the following tests
data_SomeIntField = (TypedExpr((InstVar((TypedExpr(This,"MyClass")),"someIntField")),"int"))
data_SomeBooleanField = (TypedExpr((InstVar((TypedExpr(This,"MyClass")),"someCharField")),"char"))
data_SomeCharField = (TypedExpr((InstVar((TypedExpr(This,"MyClass")),"someBooleanField")),"boolean"))
data_SomeStringField = (TypedExpr((InstVar((TypedExpr(This,"MyClass")),"someStringField")),"String"))
data_SomeObjectField = (TypedExpr((InstVar((TypedExpr(This,"MyClass")),"someObjectField")),"Object"))

data_SomeLocalIntVar= (TypedExpr((LocalVar("someLocalIntVar")),"int"))
data_SomeLocalCharVar= (TypedExpr((LocalVar("someLocalCharVar")),"char"))
data_SomeLocalBooleanVar= (TypedExpr((LocalVar("someLocalBooleanVar")),"boolean"))
data_SomeLocalStringVar= (TypedExpr((LocalVar("someLocalStringVar")),"String"))

data_ExampleInt= (TypedExpr(Integer(99),"int"))
data_ExampleChar= (TypedExpr(Char('a'),"char"))
data_ExampleBoolean= (TypedExpr(Bool(True),"boolean"))
data_ExampleString= (TypedExpr(String("ExampleTest"),"String"))

--Assing local var not return
test_Assign_LocalVar_Int_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalIntVar,data_ExampleInt) ,"void")))
test_Assign_LocalVar_Char_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalCharVar,data_ExampleChar) ,"void")))
test_Assign_LocalVar_Boolean_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalBooleanVar,data_ExampleBoolean) ,"void")))
test_Assign_LocalVar_String_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalStringVar,data_ExampleString) ,"void")))
test_Assign_LocalVar_SomeIntField_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalIntVar,data_SomeIntField) ,"void")))

--Assign local var return
test_Assign_LocalVar_Int_int = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalIntVar,data_ExampleInt) ,"int")))
test_Assign_LocalVar_Char_char = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalCharVar,data_ExampleChar) ,"char")))
test_Assign_LocalVar_Boolean_boolean = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalBooleanVar,data_ExampleBoolean) ,"boolean")))
test_Assign_LocalVar_String_String = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalStringVar,data_ExampleString) ,"String")))
test_Assign_LocalVar_SomeIntField_int = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeLocalIntVar,data_SomeIntField) ,"int")))

--Assign field no return
test_Assign_IntField_Int_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_ExampleInt) ,"void")))
test_Assign_CharField_Char_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeCharField,data_ExampleChar) ,"void")))
test_Assign_BooleanField_Boolean_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeBooleanField,data_ExampleBoolean) ,"void")))
test_Assign_StringField_String_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeStringField,data_ExampleString) ,"void")))
test_Assign_IntField_IntVar_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_SomeLocalIntVar) ,"void")))
test_Assign_IntField_IntField_void = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_SomeIntField) ,"void")))

--Assign field return
test_Assign_IntField_Int_int = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_ExampleInt) ,"int")))
test_Assign_CharField_Char_char = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeCharField,data_ExampleChar) ,"char")))
test_Assign_BooleanField_Boolean_boolean = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeBooleanField,data_ExampleBoolean) ,"boolean")))
test_Assign_StringField_String_String = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeStringField,data_ExampleString) ,"String")))
test_Assign_IntField_IntVar_int = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_SomeLocalIntVar) ,"int")))
test_Assign_IntField_IntField_int = (generateStmtExprCode dummyLookup (TypedStmtExpr(Assign(data_SomeIntField,data_SomeIntField) ,"int")))

--New 
data_New_Object_NoPara = (TypedStmtExpr(New("Object", ([])),"Object"))
data_New_Object_FieldAndFarParaInt = (TypedStmtExpr(New("Object", ([data_SomeIntField,data_SomeLocalIntVar])),"Object"))
data_New_Object_FieldAndFarParaString = (TypedStmtExpr(New("Object", ([data_SomeStringField,data_SomeLocalStringVar])),"Object"))

test_New_Object_NoPara = (generateStmtExprCode dummyLookup data_New_Object_NoPara)
test_New_Object_FieldAndFarParaInt = (generateStmtExprCode dummyLookup data_New_Object_FieldAndFarParaInt)
test_New_Object_FieldAndFarParaString = (generateStmtExprCode dummyLookup data_New_Object_FieldAndFarParaString)

--Assign new 
test_Assign_ObjectField_NewObject_void = (generateStmtExprCode dummyLookup )

--Method call
data_methodCallNoPara = (TypedStmtExpr(MethodCall(data_SomeObjectField, "myMethod", []),"Object"));
data_methodCallWithParams = (TypedStmtExpr(MethodCall(data_SomeObjectField, "myMethod", [data_SomeStringField,data_SomeLocalStringVar]),"Object"));
data_New_ObjectExpr = TypedExpr((StmtExprExpr(data_New_Object_FieldAndFarParaString)),"Object")
data_methodCallWithParamsOnNewObjectWithParams = (TypedStmtExpr(MethodCall(data_New_ObjectExpr, "myMethod", [data_SomeStringField,data_SomeLocalStringVar]),"Object"))

test_methodCallNoPara_Object = (generateStmtExprCode dummyLookup data_methodCallNoPara)
test_methodCallWithParams_Object = (generateStmtExprCode dummyLookup data_methodCallWithParams)
test_methodCallWithParams_Object_OnNewObject = (generateStmtExprCode dummyLookup data_methodCallWithParamsOnNewObjectWithParams)

example1AssignBoolAbsSyn= (TypedStmtExpr (Assign (TypedExpr (LocalVar "d","boolean"),TypedExpr (Bool True,"boolean")),"boolean"))
test_example1AssignBoolAbsSyn= generateStmtExprCode dummyLookup example1AssignBoolAbsSyn

example1AssignStringAbsSyn= (TypedStmtExpr (Assign (TypedExpr (LocalVar "a","java/lang/String"),TypedExpr (String "abcd","java/lang/String")),"boolean"))
test_example1AssignStringAbsSyn = generateStmtExprCode dummyLookup example1AssignStringAbsSyn

example1AssignIntAbsSyn= (TypedStmtExpr (Assign (TypedExpr (LocalVar "b","int"),TypedExpr (Integer 42,"int")),"boolean"))
test_example1AssignIntAbsSyn = generateStmtExprCode dummyLookup example1AssignIntAbsSyn

example1AssignCharAbsSyn= (TypedStmtExpr (Assign (TypedExpr (LocalVar "c","char"),TypedExpr (Char 'c',"char")),"boolean"))
test_example1AssignCharAbsSyn = generateStmtExprCode dummyLookup example1AssignCharAbsSyn


