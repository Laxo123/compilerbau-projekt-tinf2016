import Abstract_syntax.AbsSyn
import CodeGen

dummyMap= (\a->(-1))
dummyLookup= (CodeGenLookups(dummyMap,\a b -> (-1),\a b -> (-1),dummyMap,dummyMap))

example5TypedSyn= TypedStmt (Block [TypedStmt (Return (TypedExpr (Integer 42,"int")),"int")],"int")
example5TypedSynNoBlock= TypedStmt (Return (TypedExpr (Integer 42,"int")),"int")
example5TypedSyn2Statements= TypedStmt (Block [TypedStmt (Return (TypedExpr (Integer 42,"int")),"int"),TypedStmt (Return (TypedExpr (Integer 42,"int")),"int")],"int")
examplel11_assignbool =    TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "d","boolean"),TypedExpr (Bool True,"boolean")),"boolean")),"boolean")
exampleVarDeclBool =    TypedStmt (LocalVarDecl ("boolean","d"),"void")
example16=TypedStmt (Block [TypedStmt (LocalVarDecl ("int","counter"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 0,"int")),"boolean")),"boolean"),TypedStmt (While (TypedExpr (Binary ("<",TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 10,"int")),"boolean"),TypedStmt (Block [TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "counter","int"),TypedExpr (Binary ("+",TypedExpr (LocalVar "counter","int"),TypedExpr (Integer 1,"int")),"int")),"boolean")),"boolean")],"boolean")),"boolean"),TypedStmt (Return (TypedExpr (LocalVar "counter","int")),"int")],"java/lang/Object")


test_return_void = generateStmtCode dummyLookup (TypedStmt(ReturnEmpty,"void"))
test_return_int = generateStmtCode dummyLookup (TypedStmt(Return(TypedExpr(Integer(10),"int")),"int"))



test_example5 = generateStmtCode dummyLookup example5TypedSyn
test_example5NoBlock= generateStmtCode dummyLookup example5TypedSynNoBlock
test_example52Statements = generateStmtCode dummyLookup example5TypedSyn2Statements
test_example11_assignbool = generateStmtCode dummyLookup examplel11_assignbool
test_example11VarDeclBool = generateStmtCode dummyLookup exampleVarDeclBool
test_example16=  generateStmtCode dummyLookup example16
test_example16ByteCode = map encodeByteCodeMnemonic test_example16

example11Full = TypedStmt (Block [TypedStmt (LocalVarDecl ("java/lang/String","a"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalOrFieldVar "a","java/lang/String"),TypedExpr (String "abcd","java/lang/String")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("int","b"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "b","int"),TypedExpr (Integer 42,"int")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("char","c"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "c","char"),TypedExpr (Char 'c',"char")),"boolean")),"boolean"),TypedStmt (LocalVarDecl ("boolean","d"),"void"),TypedStmt (StmtExprStmt (TypedStmtExpr (Assign (TypedExpr (LocalVar "d","boolean"),TypedExpr (Bool True,"boolean")),"boolean")),"boolean")],"java/lang/Object")
test_example11Full = generateStmtCode dummyLookup example11Full
