import ClassGen
import Libraries.Data.ClassFormat
import Libraries.BinaryClass

emptyClassCp = [
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "EmptyClass", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
            ]

emptyClassFields=[]

emptyClassMethods=[
                Method_Info {
                af_mi = AccessFlags [],
                index_name_mi = 4,
                index_descr_mi = 5,
                tam_mi = 1,
				--aload_0,invokespecial(0,1),return
                array_attr_mi = [AttributeCode {index_name_attr = 6, tam_len_attr = 17, len_stack_attr = 1, len_local_attr = 1, tam_code_attr = 5, array_code_attr = [42,183,0,1,177], tam_ex_attr = 0, array_ex_attr = [], tam_atrr_attr = 0, array_attr_attr = []}]
                }
                ]




emptyClassStruct = generateClassFileStruct emptyClassCp emptyClassFields emptyClassMethods


encodeEmptyClass = encodeClassFile "EmptyClass.class" emptyClassStruct

