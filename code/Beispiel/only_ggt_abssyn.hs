testMethod = Method("int", 
			"methodFromGGT", 
			[("int","a"), ("int","b"), ("String","k")], 
			Block([
				LocalVarDecl("int", "h"),
				StmtExprStmt(Assign(LocalVar("h"), Integer(45))),
				LocalVarDecl("int", "j"),
				StmtExprStmt(Assign(LocalVar("j"), Integer(450000))),
				LocalVarDecl("String", "l"),
				StmtExprStmt(Assign(LocalVar("j"), String("Test"))),
				If(
					Binary("==", LocalVar("a"), LocalVar("b")), 
					Return(LocalVar("a")), 
					Just (If(
						Binary(">", LocalVar("a"), LocalVar("b")), 
						Return(StmtExprExpr(MethodCall(This, "methodFromGGT", [
							Binary("-", LocalVar("a"), LocalVar("b")), 
							LocalVar("b"), 
							LocalVar("k")
						]))), 
						Just (Return(StmtExprExpr(MethodCall(This, "methodFromGGT", [
							Binary("-", LocalVar("b"), LocalVar("a")), 
							LocalVar("a"), 
							LocalVar("k")
						]))))
					))
				)
			])
		    )