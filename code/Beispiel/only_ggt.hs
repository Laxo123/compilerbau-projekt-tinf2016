import CodeGen


ClassFile
{
magic = Magic,
minver = MinorVersion {numMinVer = 0},
maxver = MajorVersion {numMaxVer = 47},
count_cp = 17,
array_cp = [
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 6, index_nameandtype_cp = 12, desc = ""},
            Integer_Info {tag_cp = TagInteger, numi_cp = 450000, desc = ""},
            String_Info {tag_cp = TagString, index_cp = 13, desc = ""},
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 5, index_nameandtype_cp = 14, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 15, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 16, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 13, cad_cp = "methodFromGGT", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 23, cad_cp = "(IILjava/lang/String;)I", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 7, index_descr_cp = 8, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Test", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 10, index_descr_cp = 11, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "GGT", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
            ],
acfg = AccessFlags [32],
this = ThisClass {index_th = 5},
super = SuperClass {index_sp = 6},
count_interfaces = 0,
array_interfaces = [],
count_fields = 0,
array_fields = [],
count_methods = 2,
array_methods = [
                 Method_Info{
                 af_mi = AccessFlags [],
                 index_name_mi = 7,
                 index_descr_mi = 8,
                 tam_mi = 1,
                 array_attr_mi = [
                                  AttributeCode {
                                  index_name_attr = 9,
                                  tam_len_attr = 17,
                                  len_stack_attr = 1,
                                  len_local_attr = 1,
                                  tam_code_attr = 5,
                                  array_code_attr = [42,183,0,1,177],
                                  tam_ex_attr = 0,
                                  array_ex_attr = [],
                                  tam_atrr_attr = 0,
                                  array_attr_attr = []
                                  }
                                  ]
                 },
                 Method_Info {
                 af_mi = AccessFlags [8],
                 index_name_mi = 10,
                 index_descr_mi = 11,
                 tam_mi = 1,
                 array_attr_mi = [
                                 AttributeCode{
                                 index_name_attr = 9,
                                 tam_len_attr = 53,
                                 len_stack_attr = 3,
                                 len_local_attr = 6,
                                 tam_code_attr = 41,
                                 array_code_attr = [16,45,62,18,2,54,4,18,3,58,5,26,27,160,0,5,26,172,26,27,164,0,12,26,27,100,27,44,184,0,4,172,27,26,100,26,44,184,0,4,172],
                                 tam_ex_attr = 0,
                                 array_ex_attr = [],
                                 tam_atrr_attr = 0,
                                 array_attr_attr = []
                                 }
                                 ]
                 }
                 ],
count_attributes = 0,
array_attributes = []
}



testMethod = Method("int",
			"methodFromGGT",
			[("int","a"), ("int","b"), ("String","k")],
			Block([
				TypedStmt(LocalVarDecl("int", "h"),"void"),
				TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("h"),"void"),TypedExpr(Integer(45),"int")),"void"),"void")),
				TypedStmt(LocalVarDecl("int", "j"),"void"),
				TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("j"),"void"),TypedExpr( Integer(450000),"int")),"void"),"void")),
				TypedStmt(LocalVarDecl("String", "l"),"void"),
				TypedStmt(StmtExprStmt(TypedStmtExpr(Assign(TypedExpr(LocalVar("j"),"void"), TypedExpr(String("Test"),"String")),"void"),"void")),
				TypedStmt(If(
					Binary("==", LocalVar("a"), LocalVar("b")),
					Return(LocalVar("a")),
					Just (If(
						Binary(">", LocalVar("a"), LocalVar("b")),
						Return(StmtExprExpr(MethodCall(This, "methodFromGGT", [
							Binary("-", LocalVar("a"), LocalVar("b")),
							LocalVar("b"),
							LocalVar("k")
						]))),
						Just (Return(StmtExprExpr(MethodCall(This, "methodFromGGT", [
							Binary("-", LocalVar("b"), LocalVar("a")),
							LocalVar("a"),
							LocalVar("k")
						]))))
					))
				),"int")
			])
		    )

