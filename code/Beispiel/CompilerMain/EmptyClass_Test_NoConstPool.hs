import Abstract_syntax.AbsSyn
import CodeGen
import ClassGen
import MethodGen
import ConstPoolLookup
import Libraries.Data.ClassFormat
import CompilerMain
import Libraries.BinaryClass

emptyClassCp = [
            MethodRef_Info {tag_cp = TagMethodRef, index_name_cp = 3, index_nameandtype_cp = 7, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 8, desc = ""},
            Class_Info {tag_cp = TagClass, index_cp = 9, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 6, cad_cp = "<init>", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 3, cad_cp = "()V", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 4, cad_cp = "Code", desc = ""},
            NameAndType_Info {tag_cp = TagNameAndType, index_name_cp = 4, index_descr_cp = 5, desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 10, cad_cp = "EmptyClass", desc = ""},
            Utf8_Info {tag_cp = TagUtf8, tam_cp = 16, cad_cp = "java/lang/Object", desc = ""}
            ]

emptyClassAbsSyn = Class("EmptyClass", [], [Method("void", "<init>",[], (TypedStmt((Block([])),"void")))])

testEmptyClass = generateClassCode emptyClassAbsSyn emptyClassCp


main = do 
       encodeClassFile "EmptyClass.class" testEmptyClass 